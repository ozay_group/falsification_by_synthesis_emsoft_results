function acc_generate_tables()
% Creates the ACC tables in the paper

con = constants_consolidated;
    
controller = 1:4;
ig_vals = 0:3;
k_vals = [600 1600 8000];
ki_vals = [200 800 4000];
MPC_vals = [2 8 20];
% addpath(genpath('../'));
Samples = {};

load('acc_boundary_sample');
Samples{1} = acc_boundary_sample;

load('acc_interior_sample');
Samples{2} = acc_interior_sample;

SampleString = {'Boundary','Interior'};
ControllerString = {'P','Pi','commaAI','MPC'};
IGString = {'aLead=0','InverseGame','MaxBrake','aLead=K(vLead-v_des)'};
Results = {};
%
Tables = {};

for ig = ig_vals
    t = [];
    for c = controller
        if c>2
            gains_length= 1;
        else
            gains_length = length(k_vals);
        end
        if c<4
            mpc_vals = 0;
        else
            mpc_vals = MPC_vals;
        end
        for gains = 1:gains_length
            for mpc_horizon = mpc_vals
                r = [];
                for s = 1:length(Samples)
                    samples = Samples{s};
                    results = struct();
                    results.not_safe = zeros(size(samples,1),1);
                    results.Trajectories = cell(size(samples,1),12);
                    results.falsified = zeros(size(samples,1),1);
                    results.hmin_violation_u = zeros(size(samples,1),1);
                    results.crashes_u = zeros(size(samples,1),1);
                    results.headway_violation_u =  zeros(size(samples,1),1);
                    results.falsification_rate_u = -1;
                    k = k_vals(gains);
                    ki = ki_vals(gains);
                    results.input_generation = ig;
                    results.controller = c;
                    results.k = k;
                    results.ki = ki;
                    falsification_u = 0;
                    if c<3
                        disp(['InputGen:',IGString{ig+1},'__Samples:',SampleString{s},'__Controller:',ControllerString{c},num2str(gains)]);
                    elseif c == 3
                        disp(['InputGen:',IGString{ig+1},'__Samples:',SampleString{s},'__Controller:',ControllerString{c}]);
                    elseif c==4
                        disp(['InputGen:',IGString{ig+1},'__Samples:',SampleString{s},'__Controller:',ControllerString{c},num2str(mpc_horizon)]);
                    end
                    for i = 1:size(samples,1)
                        x0 = samples(i,:)';
                        [T,X,FW,FWS,AL,is_supervised,~] = acc_simulation(...
                            x0, 0, c, ig, 0, k, ki, mpc_horizon, zeros(3000,1));
                        results.Trajectories{i,1} = T;
                        results.Trajectories{i,2} = X;
                        results.Trajectories{i,3} = FW;
                        results.Trajectories{i,4} = FWS;
                        results.Trajectories{i,5} = AL;
                        results.Trajectories{i,6} = is_supervised;
                        is_successful = 1;
                        if flag
                            results.not_safe(i) = 1; 
                        end
                        if min(X(2,:)) < con.h_min - 1e-6
                            results.hmin_violation_u(i) = 1;
                            is_successful = 0;
                        end
                        if min(X(2,:)) < 1e-6
                            results.crashes_u(i) = 1;
                            is_successful = 0;
                        end
                        if min(X(2,:)./X(1,:)) < con.tau_min - 1e-6
                            results.headway_violation_u(i) = 1;
                            is_successful = 0;
                        end
                        if is_successful == 0
                            results.falsified(i) = 1;
                            falsification_u = falsification_u + 1; 
                        end

                    end
                    results.num_nonsafe = sum(results.not_safe);
                    results.num_crashes_unsupervised = sum(results.crashes_u)/length(samples);
                    results.num_headway_unsupervised = sum(results.headway_violation_u)/length(samples);
                    results.num_hmin_violation_u = sum(results.hmin_violation_u)/length(samples);
                    results.falsification_rate_u = falsification_u/length(samples);

                    Results{end+1} = results;
                    disp(['    type1: ', num2str(results.num_headway_unsupervised)]);
                    disp(['    type2: ', num2str(results.num_headway_unsupervised)]);
                    disp(['    type3: ', num2str(results.num_crashes_unsupervised)]);
                     disp(['   Total: ', num2str(results.falsification_rate_u)]);
                    r = [r results.num_headway_unsupervised ...
                        results.num_headway_unsupervised ...
                        results.num_crashes_unsupervised....
                        results.falsification_rate_u];
                    save('acc_sweep_comma');
                end
            end
            t = [t;r];
        end
    end
    Tables{ig+1} = t;
end

1;
