function acc_sweep(Samples, controller, ig_vals, k_vals, ki_vals, MPC_vals)
% Samples
    % initial conditions \in R^3
% controller
    % 1: P
    % 2: PI
    % 3: commaAI
    % 4: MPC
% ig_vals
    % 0: no input generation (aLead = 0)
    % 1: inverse game
    % 2: max brake
    % 3: aLead = K(v_des - vLead)
% k_vals/ki_vals/MPC_vals
    % values for P/PI/MPC controllers
    
addpath(genpath('../'));
con = constants_consolidated;
   
Results = {};
%
for ig = ig_vals
    for c = controller
        if c>2
            gains_length= 1;
        else
            gains_length = length(k_vals);
        end
        if c<4
            mpc_vals = 0;
        else
            mpc_vals = MPC_vals;
        end
        for gains = 1:gains_length
            for s = 1:length(Samples)
                for mpc_horizon = mpc_vals
                    samples = Samples{s};
                    results = struct();
                    results.not_safe = zeros(size(samples,1),1);
                    results.Trajectories = cell(size(samples,1),12);
                    results.falsified = zeros(size(samples,1),1);
                    results.hmin_violation_u = zeros(size(samples,1),1);
                    results.crashes_u = zeros(size(samples,1),1);
                    results.headway_violation_u =  zeros(size(samples,1),1);
                    results.falsification_rate_u = -1;
                    k = k_vals(gains);
                    ki = ki_vals(gains);
                    results.sample_index = s;
                    results.samples = samples;
                    results.input_generation = ig;
                    results.controller = c;
                    results.k = k;
                    results.ki = ki;
                    falsification_u = 0;
                    if c<3
                        1;
                        %disp(['InputGen:',IGString{ig+1},'__Samples:',SampleString{s},'__Controller:',ControllerString{c},num2str(gains)]);
                    elseif c == 3
                        1;
                        %disp(['InputGen:',IGString{ig+1},'__Samples:',SampleString{s},'__Controller:',ControllerString{c}]);
                    elseif c==4
                        1;
                        %disp(['InputGen:',IGString{ig+1},'__Samples:',SampleString{s},'__Controller:',ControllerString{c},num2str(mpc_horizon)]);
                    end
                    for i = 1:size(samples,1)
                        x0 = samples(i,:)';
                        %i
                        [T,X,FW,FWS,AL,is_supervised,~] = acc_simulation(...
                            x0, 0, c, ig, 0, k, ki, mpc_horizon, zeros(3000,1));
                        results.Trajectories{i,1} = T;
                        results.Trajectories{i,2} = X;
                        results.Trajectories{i,3} = FW;
                        results.Trajectories{i,4} = FWS;
                        results.Trajectories{i,5} = AL;
                        results.Trajectories{i,6} = is_supervised;
                        is_successful = 1;
                        if flag
                            results.not_safe(i) = 1; 
                        end
                        if min(X(2,:)) < con.h_min - 1e-6
                            results.hmin_violation_u(i) = 1;
                            is_successful = 0;
                        end
                        if min(X(2,:)) < 1e-6
                            results.crashes_u(i) = 1;
                            is_successful = 0;
                        end
                        if min(X(2,:)./X(1,:)) < con.tau_min - 1e-6
                            results.headway_violation_u(i) = 1;
                            is_successful = 0;
                        end
                        if is_successful == 0
                            results.falsified(i) = 1;
                            falsification_u = falsification_u + 1; 
                        end

                    end
                    results.num_nonsafe = sum(results.not_safe);
                    results.num_crashes_unsupervised = sum(results.crashes_u)/length(samples);
                    results.num_headway_unsupervised = sum(results.headway_violation_u)/length(samples);
                    results.num_hmin_violation_u = sum(results.hmin_violation_u)/length(samples);
                    results.falsification_rate_u = falsification_u/length(samples);

                    Results{end+1} = results;
                    disp(['    type1: ', num2str(results.num_headway_unsupervised)]);
                    disp(['    type2: ', num2str(results.num_hmin_violation_u)]);
                    disp(['    type3: ', num2str(results.num_crashes_unsupervised)]);
                     disp(['   Total: ', num2str(results.falsification_rate_u)]);

                    save('acc_sweep');
                end
            end
        end
    end
end

1;
