clear all;
Samples = {};

load('acc_boundary_sample');
Samples{1} = acc_boundary_sample;


[falsified_p1, is_interesting_p1] = acc_sweep_sTaliro(Samples{1}, 1, 600, 200, 0);
save('staliro_boundary');

[falsified_pi1, is_interesting_pi1] = acc_sweep_sTaliro(Samples{1}, 2, 600, 200, 0);
save('staliro_boundary');

[falsified_p2, is_interesting_p2] = acc_sweep_sTaliro(Samples{1}, 1, 1800, 400, 0);
save('staliro_boundary');

[falsified_pi2, is_interesting_pi2] = acc_sweep_sTaliro(Samples{1}, 2, 1800, 400, 0);
save('staliro_boundary');

[falsified_p3, is_interesting_p3] = acc_sweep_sTaliro(Samples{1}, 1, 4000, 2000, 0);
save('staliro_boundary');

[falsified_pi3, is_interesting_pi3] = acc_sweep_sTaliro(Samples{1}, 2, 4000, 2000, 0);

save('staliro_boundary');

[falsified_comma, is_interesting_comma] = acc_sweep_sTaliro(Samples{1}, 3, 600, 200, 0);
save('staliro_boundary');
