clear all;close all;clc;

[x0_p1, falsified_p1, is_interesting_p1, results_p1, history_p1] = ...
    acc_comma_ai_falsification(0, 1, 600, 200, 2, []);
save('staliro_linear_cp7')

[x0_pi1, falsified_pi1, is_interesting_pi1, results_pi1, history_pi1] = ...
    acc_comma_ai_falsification(0, 2, 600, 200, 2, []);
save('staliro_linear_cp7')

[x0_p2, falsified_p2, is_interesting_p2, results_p2, history_p2] = ...
    acc_comma_ai_falsification(0, 1, 1800, 400, 2, []);
save('staliro_linear_cp7')

[x0_pi2, falsified_pi2, is_interesting_pi2, results_pi2, history_pi2] = ...
    acc_comma_ai_falsification(0, 2, 1800, 400, 2, []);
save('staliro_linear_cp7')

[x0_p3, falsified_p3, is_interesting_p3, results_p3, history_p3] = ...
    acc_comma_ai_falsification(0, 1, 4000, 2000, 2, []);
save('staliro_linear_cp7')

[x0_pi3, falsified_pi3, is_interesting_pi3, results_pi3, history_pi3] = ...
    acc_comma_ai_falsification(0, 2, 4000, 2000, 2, []);
save('staliro_linear_cp7')

[x0_comma, falsified_comma, is_interesting_comma, results_comma, history_comma] = ...
    acc_comma_ai_falsification(0, 3, 600, 200, 2, []);
save('staliro_linear_cp7')

