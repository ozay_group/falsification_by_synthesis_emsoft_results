P = CInv_LK;
% Find the convex hull of P
PU = PolyUnion(P);
CP = PU.convexHull();
% Find LJ-ellipsoid of the convex hull - 
% this is the minimum volume ellipsoid covering a convex shape
X_mat = find_lj(CP);

% dynamics
f = struct();
f.A=[0 1 con.u0 0;
  0 -(con.Caf+con.Car)/(con.m*con.u0) 0 ((con.b*con.Car-con.a*con.Caf)/(con.m*con.u0) - con.u0);
  0 0 0 1;
  0 (con.b*con.Car-con.a*con.Caf)/(con.Iz*con.u0)  0 -(con.a^2 * con.Caf + con.b^2 * con.Car)/(con.Iz*con.u0)];

f.B=[0;con.Caf/con.m; 0; con.a*con.Caf/con.Iz];

f.F=[0;0;-1;0];

f.AD = double(expm(f.A*Dt_comma));

syms t
f.BD = double(real(int(expm(f.A*t), t, 0, Dt_comma)*f.B));

f.FD = double(real(int(expm(f.A*t), t, 0, Dt_comma)*f.F));

% input polytope

U = Polyhedron('ub',con.d_max,'lb',con.d_min);
UV = U.V;
[Adt, Bdt, Edt, Kdt, Adtc, Bdtc, Edtc, Kdtc, Adtcrd, Bdtcrd, Edtcrd, Kdtcrd, Adt_tau, Bdt_tau, Edt_tau, Kdt_tau] = gen_dt_mat(con, Dt, Dt_comma);
find_Cinv_xu;