% 1. Supervised? 2. Controller type 3. Input gen type 4. Integrator IC 
% 5. Boundary samples? 6. Plotting? 7. Controller gains
% 8. MPC horizon?

P_gains = {[-0.0101   -0.1470   -0.5165   -0.3118], ...
  [-1.3643   -0.0877   -8.5586   -0.3508],...
  [-0.4518   -0.0741   -2.8867   -0.1824]};
PI_gains = {[-0.0876   -0.0877   -1.6501   -0.3927   -0.0030], ...
  [-2.6481   -0.2181   -9.9572   -0.1631   -0.4093], ...
  [-1.1429   -0.1216   -4.2711   -0.1556   -0.1878]};

%% Table 8, Lines 1-3
run_closed_loop_func(0, 'P', 'off', 0, 0, 0, P_gains{1}, [])
run_closed_loop_func(0, 'P', 'heuristic', 0, 0, 0, P_gains{1}, [])
run_closed_loop_func(0, 'P', 'ig+e', 0, 0, 0, P_gains{1}, [])

run_closed_loop_func(0, 'P', 'off', 0, 1, 0, P_gains{1}, [])
run_closed_loop_func(0, 'P', 'heuristic', 0, 1, 0, P_gains{1}, [])
run_closed_loop_func(0, 'P', 'ig+e', 0, 1, 0, P_gains{1}, [])

run_closed_loop_func(0, 'P', 'off', 0, 0, 0, P_gains{2}, [])
run_closed_loop_func(0, 'P', 'heuristic', 0, 0, 0, P_gains{2}, [])
run_closed_loop_func(0, 'P', 'ig+e', 0, 0, 0, P_gains{2}, [])

run_closed_loop_func(0, 'P', 'off', 0, 1, 0, P_gains{2}, [])
run_closed_loop_func(0, 'P', 'heuristic', 0, 1, 0, P_gains{2}, [])
run_closed_loop_func(0, 'P', 'ig+e', 0, 1, 0, P_gains{2}, [])

run_closed_loop_func(0, 'P', 'off', 0, 0, 0, P_gains{3}, [])
run_closed_loop_func(0, 'P', 'heuristic', 0, 0, 0, P_gains{3}, [])
run_closed_loop_func(0, 'P', 'ig+e', 0, 0, 0, P_gains{3}, [])

run_closed_loop_func(0, 'P', 'off', 0, 1, 0, P_gains{3}, [])
run_closed_loop_func(0, 'P', 'heuristic', 0, 1, 0, P_gains{3}, [])
run_closed_loop_func(0, 'P', 'ig+e', 0, 1, 0, P_gains{3}, [])

%% Table 8, Lines 4-6

run_closed_loop_func(0, 'PI', 'off', 0, 0, 0, PI_gains{1}, [])
run_closed_loop_func(0, 'PI', 'heuristic', 0, 0, 0, PI_gains{1}, [])
run_closed_loop_func(0, 'PI', 'ig+e', 0, 0, 0, PI_gains{1}, [])

run_closed_loop_func(0, 'PI', 'off', 0, 1, 0, PI_gains{1}, [])
run_closed_loop_func(0, 'PI', 'heuristic', 0, 1, 0, PI_gains{1}, [])
run_closed_loop_func(0, 'PI', 'ig+e', 0, 1, 0, PI_gains{1}, [])

run_closed_loop_func(0, 'PI', 'off', 0, 0, 0, PI_gains{2}, [])
run_closed_loop_func(0, 'PI', 'heuristic', 0, 0, 0, PI_gains{2}, [])
run_closed_loop_func(0, 'PI', 'ig+e', 0, 0, 0, PI_gains{2}, [])

run_closed_loop_func(0, 'PI', 'off', 0, 1, 0, PI_gains{2}, [])
run_closed_loop_func(0, 'PI', 'heuristic', 0, 1, 0, PI_gains{2}, [])
run_closed_loop_func(0, 'PI', 'ig+e', 0, 1, 0, PI_gains{2}, [])

run_closed_loop_func(0, 'PI', 'off', 0, 0, 0, PI_gains{3}, [])
run_closed_loop_func(0, 'PI', 'heuristic', 0, 0, 0, PI_gains{3}, [])
run_closed_loop_func(0, 'PI', 'ig+e', 0, 0, 0, PI_gains{3}, [])

run_closed_loop_func(0, 'PI', 'off', 0, 1, 0, PI_gains{3}, [])
run_closed_loop_func(0, 'PI', 'heuristic', 0, 1, 0, PI_gains{3}, [])
run_closed_loop_func(0, 'PI', 'ig+e', 0, 1, 0, PI_gains{3}, [])

%% Table 8, Lines 7-9

run_closed_loop_func(0, 'MPC', 'off', 0, 0, 0, [], 2)
run_closed_loop_func(0, 'MPC', 'heuristic', 0, 0, 0, [], 2)
run_closed_loop_func(0, 'MPC', 'ig+e', 0, 0, 0, [], 2)

run_closed_loop_func(0, 'MPC', 'off', 0, 1, 0, [], 2)
run_closed_loop_func(0, 'MPC', 'heuristic', 0, 1, 0, [], 2)
run_closed_loop_func(0, 'MPC', 'ig+e', 0, 1, 0, [], 2)

run_closed_loop_func(0, 'MPC', 'off', 0, 0, 0, [], 5)
run_closed_loop_func(0, 'MPC', 'heuristic', 0, 0, 0, [], 5)
run_closed_loop_func(0, 'MPC', 'ig+e', 0, 0, 0, [], 5)

run_closed_loop_func(0, 'MPC', 'off', 0, 1, 0, [], 5)
run_closed_loop_func(0, 'MPC', 'heuristic', 0, 1, 0, [], 5)
run_closed_loop_func(0, 'MPC', 'ig+e', 0, 1, 0, [], 5)

run_closed_loop_func(0, 'MPC', 'off', 0, 0, 0, [], 20)
run_closed_loop_func(0, 'MPC', 'heuristic', 0, 0, 0, [], 20)
run_closed_loop_func(0, 'MPC', 'ig+e', 0, 0, 0, [], 20)

run_closed_loop_func(0, 'MPC', 'off', 0, 1, 0, [], 20)
run_closed_loop_func(0, 'MPC', 'heuristic', 0, 1, 0, [], 20)
run_closed_loop_func(0, 'MPC', 'ig+e', 0, 1, 0, [], 20)

%% Table 10, left half

run_closed_loop_func(0, 'commaAI', 'off', 0, 0, 0, [], [])
run_closed_loop_func(0, 'commaAI', 'heuristic', 0, 0, 0, [], [])
run_closed_loop_func(0, 'commaAI', 'ig+e', 0, 0, 0, [], [])

run_closed_loop_func(0, 'commaAI', 'off', 0, 1, 0, [], [])
run_closed_loop_func(0, 'commaAI', 'heuristic', 0, 1, 0, [], [])
run_closed_loop_func(0, 'commaAI', 'ig+e', 0, 1, 0, [], [])

%% Table 10, right half

run_closed_loop_func(0, 'commaAIstar', 'off', 0, 0, 0, [], [])
run_closed_loop_func(0, 'commaAIstar', 'heuristic', 0, 0, 0, [], [])
run_closed_loop_func(0, 'commaAIstar', 'ig+e', 0, 0, 0, [], [])

run_closed_loop_func(0, 'commaAIstar', 'off', 0, 1, 0, [], [])
run_closed_loop_func(0, 'commaAIstar', 'heuristic', 0, 1, 0, [], [])
run_closed_loop_func(0, 'commaAIstar', 'ig+e', 0, 1, 0, [], [])