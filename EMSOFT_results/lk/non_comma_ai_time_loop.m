for t = t_vals 
  % Getting legacy controls
  if strcmp(controller, 'MPC')
    u_t = controller_MPC_LK(x_t, d_t, con, Dt, horizon);
  elseif strcmp(controller, 'PI')
    [xi_t, u_t] = controller_PI(x_t, xi_t, con, Dt, gains);
  elseif strcmp(controller, 'P')
    u_t = controller_P(x_t, con, Dt, gains);
  end
  u_legacy_vals = [u_legacy_vals, u_t];
  % Getting supervised controls
  if is_supervised
    u_t_super = supervisor_react(u_t, x_t, d_t, con, CInv_LK);
    u_overwritten_vals = [u_overwritten_vals, u_t_super];
    x_tp1 = fx_LK(x_t, u_t_super, d_t, Adt, Bdt, Edt, Kdt, Dt);
  else
    u_overwritten_vals = [u_overwritten_vals, u_t];
    x_tp1 = fx_LK(x_t, u_t, d_t, Adt, Bdt, Edt, Kdt, Dt);
  end
  x_vals = [x_vals, x_t];
  d_vals = [d_vals, d_t];
  % Getting disturbance input
  if strcmp(input_gen, 'off')
    d_t = 0;
  elseif strcmp(input_gen, 'heuristic')
    if x_tp1(1) > x_t(1)
        d_t = con.d_min;
    else
        d_t = con.d_max;
    end
  elseif strcmp(input_gen, 'ig+e')
    [d_t, next_idx] = inverse_game_LK_fast(x_t, BR_xd_LK_001, BR_x_LK_001, next_idx);
    if isempty(d_t) 
      d_t = find_input(x_t, f, UV, X_mat, u_t);
    end
  end
  x_t = x_tp1;
end