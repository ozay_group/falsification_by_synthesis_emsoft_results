plot(t_vals(1:size(x_vals, 2)), x_vals(1,:), 'Linewidth',2,'Color','k');
hold on; 
plot([0,T],[0.9,0.9], 'Linewidth',2,'Color',[1, 0, 0], 'Linestyle','--');
plot([0,T],[-0.9,-0.9], 'Linewidth',2,'Color',[1, 0, 0], 'Linestyle','--');
axis([0,T,-1,1]);
xlabel('Time (sec)')
ylabel('Deviation from centerline (m)')
title(['Iteration ' num2str(sample)])
hold on;
drawnow