for t = t_vals 
  % Get controls
  theta_ego = R2D*x_t(3);
  rd = rd2road(init_road, Rd, con.u0);
  observed_path = lane_boundaries_new(x_t(1), x_t(3), rd, con.u0);
  response = py_controller.update(v_ego, theta_ego, observed_path);
  steer = response{1};
  u_t = con.u_max_LK*steer;
  u_legacy_vals = [u_legacy_vals, u_t];
  % Supervision
  if is_supervised
    if mod(t, 0.1) == 0
      u_t_super = supervisor(u_t, x_t, CInv_LK_xu, CInv_LK);
    end
    u_overwritten_vals = [u_overwritten_vals, u_t_super];
    x_tp1 = fx_LK(x_t, u_t_super, d_t, Adtc, Bdtc, Edtc, Kdtc, Dt_comma);
  else
    x_tp1 = fx_LK(x_t, u_t, d_t, Adtc, Bdtc, Edtc, Kdtc, Dt_comma);
    u_overwritten_vals = [u_overwritten_vals, u_t];
  end
  x_vals = [x_vals, x_t];
  d_vals = [d_vals, d_t];
  % Disturbance generation
  if strcmp(input_gen, 'off')
    d_t = 0;
    Rd = Rd_zero;
  elseif strcmp(input_gen, 'min')
    d_t = con.d_min;
    Rd = d_t*Rd_ones;
  elseif strcmp(input_gen, 'max')
    d_t = con.d_max;
    Rd = d_t*Rd_ones;
  elseif strcmp(input_gen, 'heuristic')
    if x_tp1(1) > x_t(1)
        d_t = con.d_min;
        Rd = d_t*Rd_ones;
    else
        d_t = con.d_max;
        Rd = d_t*Rd_ones;
    end
  elseif strcmp(input_gen, 'ig+e')
    full_len = length(Rd);
    Rd = Rd(2:end-fog_horizon); % fog horizon from 1 to n
    old_len = length(Rd);
    x_t_hyp = x_t;
    for timestep = 1:length(Rd)
      x_t_hyp = fx_LK(x_t_hyp, u_t, Rd(timestep), Adtcrd, Bdtcrd, Edtcrd, Kdtcrd, 1/con.u0);
    end
    for timestep = old_len+1:full_len
      [d_t, next_idx] = inverse_game_LK_fast(x_t_hyp, BR_xd_LK_001, BR_x_LK_001, next_idx);
      if isempty(d_t) 
        d_t = find_input(x_t_hyp, f, UV, X_mat, u_t);
      end
      Rd(timestep) = d_t;
      x_t_hyp = fx_LK(x_t_hyp, u_t, d_t, Adtcrd, Bdtcrd, Edtcrd, Kdtcrd, 1/con.u0);
    end
  end
  % Next state update
  x_t = x_tp1;
end