function run_closed_loop_func(is_supervised, controller, input_gen, integrator_ic, boundary, plotting, gains, horizon)
%% PARAMETERS 
if nargin < 7
  plotting = 0;
  gains = [];
  horizon = [];
elseif nargin < 8
  gains = [];
  horizon = [];
end
num_samples = 500;
fog_horizon = 0;
R2D = (180/pi);
init_road = [0 0 0]';
T = 30;
Dt = 0.1;
Dt_comma = 0.01;
rd_len = 5*55;

%% Setup
addpath(genpath(pwd))
load('CInv_LK');
load('BR_xd_LK_001');
load('BR_x_LK_001');
con = constants_consolidated;
if boundary == 1
  load('lk_boundary_consolidated')
  lk_boundary = lk_boundary(1:num_samples, :);
elseif boundary == 0
  load('lk_interior_consolidated')
end

Rd_zero = zeros(rd_len, 1);
Rd_ones = ones(rd_len, 1);

input_gen_setup

unsafe = zeros(size(lk_boundary, 1), 1);
unsafe_all = zeros(size(lk_boundary, 1), 1);
next_idx = [];
if plotting
  figure;
end

%% Main loop

for int_ic = integrator_ic
  for sample = 1:size(lk_boundary, 1)
    x0 = lk_boundary(sample,:)';
    d0 = 0;
    x_vals = [];
    d_vals = [];
    u_legacy_vals = [];
    u_overwritten_vals = [];
    x_t = x0; % initial state
    d_t = d0; % initial disturbance
    xi_t = int_ic; % integrator state
    %% Computing rollout
    if strcmp('commaAI', controller) || strcmp('commaAIstar', controller)
      comma_ai_init
      comma_ai_time_loop
    else
      t_vals = 0:Dt:T;
      non_comma_ai_time_loop
    end
    %% Compute statistics (for rollout)
    if(any(abs(x_vals(1,:)) > 0.9+1e-6))
      unsafe(sample) = 1;
    end
    if(any(any(x_vals > con.x_max+1e-6, 2) | any(x_vals < con.x_min-1e-6, 2)))
      unsafe_all(sample) = 1;
    end

  %% PLOTTING
  if plotting
    plotting_code
  end
  end
end
%% Compute statistics (total)
unsafe_frac = sum(unsafe)/length(unsafe);
unsafe_all_frac = sum(unsafe_all)/length(unsafe_all);
disp('---------------------------------------------')
disp(['Supervised: ', num2str(is_supervised), '; Controller: ', controller, '; Input generation: ', input_gen, '; Integrator IC: ' num2str(integrator_ic), '; Boundary: ', num2str(boundary), '; Gains: ', mat2str(gains), '; Horizon: ', num2str(horizon)])
disp('---------------------------------------------')
disp(['Fraction leaving lane bounds: ', num2str(unsafe_frac), ' = ', num2str(sum(unsafe)), '/', num2str(length(unsafe))])
disp(['Fraction leaving any bounds: ', num2str(unsafe_all_frac), ' = ', num2str(sum(unsafe_all)), '/', num2str(length(unsafe_all))])