function X = find_lj(P)
% finds LJ ellipsoid for convex set P
% LJ ellipsoid is given by [x' 1]*X*[x;1];
V = P.V;
m = size(V,1);
N = size(V,2);

X = sdpvar(N+1);
f = X>=0;

for r = 1:m
    v = V(r,:)';
    f = [f; [v' 1]*X*[v;1] <= 1];
end

options = sdpsettings('verbose',0);
sol = optimize(f, -log(det(X)), options);
X = value(X);
