function u = find_input(xi, f, U, X, car_input)
% FIND_INPUT finds an input u that forces x outside of P
% ------------
% INPUTS
% xi: initial state
% f: dynamics (x(t+1) = f.Ax(t) + f.Bu(t) + f.F)
% U: corners of admissible input polytope where each row is a corner
% X: ellipsoid matrix
% -------------
% OUTPUTS
% u
% -----------

maxC = 0;
u = NaN;
for r = 1:size(U,1)
    xf = f.AD*xi + f.FD*U(r,:)' + f.BD*car_input;
    Cr = [xf' 1]*X*[xf;1];
    if Cr > maxC
        u = U(r,:);
        maxC = Cr;
    end
end
