function [u_t_overwritten, u1, u2] = supervisor(u_t_legacy, x_t, safe_set_xu, safe_set_x)

if safe_set_x.contains(x_t)
    safe_set_x.minHRep;
    u_interval = Polyhedron('A',[safe_set_xu.A;[eye(4),zeros(4,1)];[-eye(4),zeros(4,1)]],...
                            'b',[safe_set_xu.b;x_t+0e-4;-x_t-0e-4]);
    if ~isEmptySet(u_interval)
        u1 = min(u_interval.V(:,5));
        u2 = max(u_interval.V(:,5));
        if u1 <= u_t_legacy && u_t_legacy <= u2
            u_t_overwritten = u_t_legacy; 
        else
            if abs(u_t_legacy - u1) <= abs(u_t_legacy - u2)
                u_t_overwritten = u1 + 0.0*(u2-u1); 
            else
                u_t_overwritten = u2 - 0.0*(u2-u1); 
            end
        end
        return;
    end
end

warning('NOT IN CINV !?')
