function dyn = get_dyn_lk(con)

	% State is [ y r psi r ]'
	%
	% y 	- lateral position wrt lane center line
	% r 	- lateral velocity
	% psi 	- yaw angle
	% r 	- yaw rate
	%

	Car = con.Car; Caf = con.Caf; a = con.a; b = con.b; u0 = con.u0;
	m = con.mass; Iz = con.Iz; dt = con.dt;

	% Continuous dynamics
    A=[0 1 u0 0; 
      0 -(Caf+Car)/(m*u0) 0 ((b*Car-a*Caf)/(m*u0) - u0); 
      0 0 0 1;
      0 (b*Car-a*Caf)/(Iz*u0)  0 -(a^2 * Caf + b^2 * Car)/(Iz*u0)];

    B=[0;Caf/m; 0; a*Caf/Iz];

    E=[0;0;1;0];

    Cy=[1 0 0 0]; % use w/ y
    c=[1 0 20 0]; % use w/ y_preview

    Sys=ss(A,B,c,0);
    Kd=.25; Kp=4;
    Q=Kp*c'*c + Kd*A'*c'*c*A;
    R=600;
    [K,Slqr,Elqr]=lqr(Sys,Q,R);
    % K = zeros(1,4);

    % Closed-loop
    SysCL=ss(A-B*K,B,Cy,0);

    A = SysCL.A;
    B = SysCL.B;

    % Integrate dynamics
    A_s = @(s) expm(s*A);
    Ad = A_s(dt);
    Bd = integral(A_s, 0, dt, 'ArrayValued', true) * B;
    Kd = zeros(4,1);
    Ed = integral(A_s, 0, dt, 'ArrayValued', true) * E;

    K = zeros(1,4);
%     XU_set = Polyhedron([-K 1; K -1], [pi/3; pi/3]);
% 
%   	XD_plus = [0 0 0 0 1.3*con.alpha_ass*con.g/con.u0];
%   	XD_minus = [0 0 0 0 -1.3*con.alpha_ass*con.g/con.u0];
    XU_set = Polyhedron([-K 1; K -1], [con.u_max_LK; con.u_max_LK]);

  	XD_plus = [0 0 0 0 con.d_max];
  	XD_minus = [0 0 0 0 con.d_min];
    dyn = Dyn(Ad,Kd,Bd,XU_set,Ed,XD_minus,XD_plus);
    % dyn.save_constant('feedback', K)
end
