function u_t_overwritten = supervisor_react(u_t_legacy, x_t, d_t, con, safe_set_x)

[A,B,E,K, X,U,~] = getABEK_XUW_LK(con);
W = Polyhedron('V',d_t);

if safe_set_x.contains(x_t)
    safe_set_x.minHRep;
    safe_set_xu = RobustCPre(safe_set_x,X,U,W,  A,B,E,K);
    u_interval = Polyhedron('A',[safe_set_xu.A;[eye(4),zeros(4,1)];[-eye(4),zeros(4,1)]],...
                            'b',[safe_set_xu.b;x_t;-x_t]);
    if ~isEmptySet(u_interval)
        u1 = min(u_interval.V(:,5));
        u2 = max(u_interval.V(:,5));
        if u1 <= u_t_legacy && u_t_legacy <= u2
            u_t_overwritten = u_t_legacy; 
        else
            if abs(u_t_legacy - u1) <= abs(u_t_legacy - u2)
                u_t_overwritten = u1 + 0.0*(u2-u1); 
            else
                u_t_overwritten = u2 - 0.0*(u2-u1); 
            end
        end
        return;
    end
end

warning('NOT IN CINV !?')