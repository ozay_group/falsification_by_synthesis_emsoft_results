function observed_path  = lane_boundaries_new(y, theta, xy, mu)
% Rotates lookahead road into the car frame
    theta = -theta;
    theta = rad2deg(theta);
    t = 1:55;
    rotated = xy;
    rot_mat = rotx(theta);
    rot_mat = rot_mat(2:end, 2:end);
    rotated_post = rot_mat*rotated;
    [b,i,~] = unique(rotated_post(1,:));
    observed_path = interp1(b, rotated_post(2,i)-y, t);
    observed_path = observed_path(1:50);
    nan_ct = isnan(observed_path);
    if ~sum(nan_ct)
        return
    elseif sum(nan_ct) == 50
        observed_path = 10*sign(theta)*t;
        return
    else
        observed_path(isnan(observed_path)) = mean(observed_path(~isnan(observed_path)));
    end
end