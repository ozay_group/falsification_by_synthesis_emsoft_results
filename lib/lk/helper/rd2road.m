function p_out = rd2road(p0, Rd, v0)

h = length(Rd);
% sequence of waypoints for the center of the lane
Pc = [p0 zeros(3,h)];

for i = 1:h
  % Computing lookahead road
  Pc(:,i+1) = rd2road2(Pc(:,i), Rd(i), v0, 100);
end
p_out = Pc(1:2, :);