function [A,B,E,K, X,U,W] = getABEK_XUW_LK(con, Dt)
if nargin < 2
    Dt = con.dt;
end

% ========= METHOD 1 ========= 
Ac = [0, 1, con.u0, 0;...
      0, -(con.Caf+con.Car)/(con.m*con.u0), 0, (con.b*con.Car-con.a*con.Caf)/(con.m*con.u0)-con.u0;...
      0, 0, 0, 1;...
      0, (con.b*con.Car-con.a*con.Caf)/(con.Iz*con.u0), 0, -(con.a^2*con.Caf + con.b^2*con.Car)/(con.Iz*con.u0)];
Bc = [0; con.Caf/con.m; 0; con.a*con.Caf/con.Iz];
Ec = [0; 0; -1; 0]; 
Kc = [0; 0; 0; 0];


sysC = ss(Ac,Bc,eye(4),0);
sysD = c2d(sysC, Dt,'zoh');
A = sysD.A;
B = sysD.B;


sysC = ss(Ac,Ec,eye(4),0);
sysD = c2d(sysC, Dt,'zoh');
E = sysD.B;

sysC = ss(Ac,Kc,eye(4),0);
sysD = c2d(sysC, Dt,'zoh');
K = sysD.B;


% ========= METHOD 2 ========= 
% Car = con.Car; Caf = con.Caf; a = con.a; b = con.b; u0 = con.u0;
% 	m = con.m; Iz = con.Iz; dt = con.dt;
% 
% 	% Continuous dynamics
%     A=[0 1 u0 0; 
%       0 -(Caf+Car)/(m*u0) 0 ((b*Car-a*Caf)/(m*u0) - u0); 
%       0 0 0 1;
%       0 (b*Car-a*Caf)/(Iz*u0)  0 -(a^2 * Caf + b^2 * Car)/(Iz*u0)];
% 
%     B=[0;Caf/m; 0; a*Caf/Iz];
% 
%     E=[0;0;-1;0];
% 
%     % Integrate dynamics
%     A_s = @(s) expm(s*A);
%     Ad = A_s(dt);
%     Bd = integral(A_s, 0, dt, 'ArrayValued', true) * B;
%     Kd = zeros(4,1);
%     Ed = integral(A_s, 0, dt, 'ArrayValued', true) * E;
%     A = Ad;
%     B = Bd;
%     E = Ed;
%     K = Kd;
% %%%

X = Polyhedron('lb',con.x_min,...
                   'ub',con.x_max); 
               
U = Polyhedron('lb',con.u_min_LK,'ub',con.u_max_LK);

W = Polyhedron('lb', con.d_min,'ub', con.d_max);



