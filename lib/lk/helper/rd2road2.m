function pnew = rd2road2(p, rd, v0, sampling_rate)
% Computes lookahead road
eps = 1e-5;
if norm(rd) < eps
  R = Inf;
  pnew = [ p(1:2) + [cos(p(3)) -sin(p(3)); sin(p(3)) cos(p(3))]*[(v0/sampling_rate) ; 0 ] ; p(3) ];
else
    R = v0/rd;
    px = R*cos((rd/sampling_rate) - (pi/2));
    py = R*(sin((rd/sampling_rate) - (pi/2))+1); 
    thetaR = rd/sampling_rate;
    pnew = [cos(p(3)) -sin(p(3)); sin(p(3)) cos(p(3))]*[px;py];
    pnew = [pnew + p(1:2); p(3) + thetaR];
end