function [Adt, Bdt, Edt, Kdt, Adtc, Bdtc, Edtc, Kdtc, Adtcrd, Bdtcrd, Edtcrd, Kdtcrd, Adt_tau, Bdt_tau, Edt_tau, Kdt_tau] = gen_dt_mat(con, Dt, Dtc)

Ac = [0, 1, con.u0, 0;...
      0, -(con.Caf+con.Car)/(con.m*con.u0), 0, (con.b*con.Car-con.a*con.Caf)/(con.m*con.u0)-con.u0;...
      0, 0, 0, 1;...
      0, (con.b*con.Car-con.a*con.Caf)/(con.Iz*con.u0), 0, -(con.a^2*con.Caf + con.b^2*con.Car)/(con.Iz*con.u0)];
Bc = [0; con.Caf/con.m; 0; con.a*con.Caf/con.Iz];
Ec = [0; 0; -1; 0]; 
Kc = [0; 0; 0; 0];

sysC = ss(Ac,Bc,eye(4),0);
sysD = c2d(sysC, Dt,'zoh');
Adt = sysD.A;
Bdt = sysD.B;

sysC = ss(Ac,Ec,eye(4),0);
sysD = c2d(sysC, Dt,'zoh');
Edt = sysD.B;

sysC = ss(Ac,Kc,eye(4),0);
sysD = c2d(sysC, Dt,'zoh');
Kdt = sysD.B;

sysC = ss(Ac,Bc,eye(4),0);
sysD = c2d(sysC, Dtc,'zoh');
Adtc = sysD.A;
Bdtc = sysD.B;

sysC = ss(Ac,Ec,eye(4),0);
sysD = c2d(sysC, Dtc,'zoh');
Edtc = sysD.B;

sysC = ss(Ac,Kc,eye(4),0);
sysD = c2d(sysC, Dtc,'zoh');
Kdtc = sysD.B;

sysC = ss(Ac,Bc,eye(4),0);
% sysD = c2d(sysC, 1/con.u0,'zoh');
sysD = c2d(sysC, 0.01,'zoh');
Adtcrd = sysD.A;
Bdtcrd = sysD.B;

sysC = ss(Ac,Ec,eye(4),0);
% sysD = c2d(sysC, 1/con.u0,'zoh');
sysD = c2d(sysC, 0.01,'zoh');
Edtcrd = sysD.B;

sysC = ss(Ac,Kc,eye(4),0);
% sysD = c2d(sysC, 1/con.u0,'zoh');
sysD = c2d(sysC, 0.01,'zoh');
Kdtcrd = sysD.B;

% Ac_torque = [0, 1, con.u0, 0, 0, 0;...
%       0, -(con.Caf+con.Car)/(con.m*con.u0), 0, (con.b*con.Car-con.a*con.Caf)/(con.m*con.u0)-con.u0, con.Caf/con.m, 0;...
%       0, 0, 0, 1, 0, 0;...
%       0, (con.b*con.Car-con.a*con.Caf)/(con.Iz*con.u0), 0, -(con.a^2*con.Caf + con.b^2*con.Car)/(con.Iz*con.u0), con.a*con.Caf/con.Iz, 0;
%       0, 0, 0, 0, 0, 1;
%       0, 0, -con.align_scale*con.u0, 0, 0, 0];
Ac_torque = [0, 1, con.u0, 0, 0, 0;...
      0, -(con.Caf+con.Car)/(con.m*con.u0), 0, (con.b*con.Car-con.a*con.Caf)/(con.m*con.u0)-con.u0, con.Caf/con.m, 0;...
      0, 0, 0, 1, 0, 0;...
      0, (con.b*con.Car-con.a*con.Caf)/(con.Iz*con.u0), 0, -(con.a^2*con.Caf + con.b^2*con.Car)/(con.Iz*con.u0), con.a*con.Caf/con.Iz, 0;
      0, 0, 0, 0, 446, -528+1;
      0, 0, -con.align_scale*con.u0, 0, 446, -528];
Bc_torque = [0; 0; 0; 0; 0; con.steer_scale];
Ec_torque = [0; 0; -1; 0; 0; 0]; 
Kc_torque = [0; 0; 0; 0; 0; 0];

sysC_tau = ss(Ac_torque,Bc_torque,eye(6),0);
sysD_tau = c2d(sysC_tau, Dtc,'zoh');
Adt_tau = sysD_tau.A;
Bdt_tau = sysD_tau.B;

sysC_tau = ss(Ac_torque,Ec_torque,eye(6),0);
sysD_tau = c2d(sysC_tau, Dtc,'zoh');
Edt_tau = sysD_tau.B;

sysC_tau = ss(Ac_torque,Kc_torque,eye(6),0);
sysD_tau = c2d(sysC_tau, Dtc,'zoh');
Kdt_tau = sysD_tau.B;

end