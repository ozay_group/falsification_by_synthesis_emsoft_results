function x_tp1 = fx_LK(x_t, u_t, d_t, Ad, Bd, Ed, Kd, Dt)

if nargin < 8
    Dt = 0.1;
end

x_tp1 = Ad*x_t + Bd*u_t + Ed*d_t + Kd;

