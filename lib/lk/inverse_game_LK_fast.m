function [w_t, next_idx] = inverse_game_LK_fast(x_t, BR_xw, BR_x, start_idx)

% function u = extractStrategy(x0, BR_xu)
% BR: array of polyhedrons, backwards reachable set
% x0: initial state

n = length(x_t);    % dim of x
m = BR_xw(1).Dim - n; % dim of u

if nargin < 4 || isempty(start_idx) || ~BR_x(start_idx).contains(x_t)
    is_inside = BR_x.contains(x_t);
    start_idx = find(is_inside,1);
end
w_t = [];

if isempty(start_idx)
    next_idx = [];
    return;
end
 
U_win = Polyhedron('A',[BR_xw(start_idx).A],'b',[BR_xw(start_idx).b],...
                   'Ae',[eye(n),zeros(n,m)],'be',x_t); 
               % sent x from BR(i) into BR(i+1)
if U_win.isEmptySet()
  next_idx = [];
  return;
end
w_t = U_win.V(1,n+1:end)';
% w_t = U_win.V(2,n+1:end)';

if ~isempty(w_t) && start_idx > 1
    next_idx = start_idx - 1;
else
    next_idx = [];
end