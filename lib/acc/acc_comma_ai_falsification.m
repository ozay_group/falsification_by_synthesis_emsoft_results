% Given a controller, this script uses sTaliro to generate 
% an initial condition and/or an input trajectory
% that will result in the violation of safety specs

function [X0, falsify_flag, is_X0_in_cinv, results, history] = acc_comma_ai_falsification(...
    my_supervisor_enabled, my_controller, my_k, my_k_i, my_mpc_horizon, myx0)
%% User Defined Parameters
plot_flag = 0;
global supervisor_enabled controller k k_i mpc_horizon my_x0 
% global InpSignals2
my_x0 = myx0;
supervisor_enabled = my_supervisor_enabled;
controller = my_controller;
k = my_k;
k_i = my_k_i;
mpc_horizon = my_mpc_horizon;
bbmdl = staliro_blackbox;
bbmdl.model_fcnptr = @acc_comma_ai_falsification_helper;
con = constants_consolidated;
% simulation time
TimeHorizon = 30;
% number of variables searched by sTaliro
cp_array = 7;
% Initial Condition [vEgo; dRel; vLead]
if ~isempty(my_x0)
    init_cond = [];
    my_runs = 1;
else
    init_cond = [con.v_min  con.v_max; 
             con.h_min  con.h_max; 
             con.vl_min con.vl_max];
     my_runs = 100;
%     cp_array = cp_array + 3;
end
% external input_range
input_range = [con.al_min con.al_max];
% specification: assume starting in the safe set and satisfying vLead>=0 at all times, 
% respect time/distance headway specs at all times
phi = '(d /\ []v)->[](h /\ d)';
% phi = '([]v)->([](h /\ d))';
% 
% Lead car does not go backwards (vLead >= 0)
ii = 1;
preds(ii).str='v';
preds(ii).A = [0 0 -1];
preds(ii).b = -con.vl_min;
% dist headway spec: dRel > min_dRel
ii = 2;
preds(ii).str='h';
preds(ii).A = [0 -1 0];
preds(ii).b = -(con.h_min-1e-6);
% time headway spec: vEgo*tau_min - dRel < 0 
ii = 3;
preds(ii).str='d';
preds(ii).A = [con.tau_min -1 0];
preds(ii).b = 1e-6;
% sTaliro options
opt = staliro_options();
opt.runs = my_runs;
n_tests = 100;
opt.spec_space = 'X';
opt.optim_params.n_tests = n_tests;
opt.interpolationtype = {'linear'};
opt.plot = 1;
opt.black_box = 1;
opt.fals_at_zero = 0;
opt.seed = 0;
if controller == 3
    opt.SampTime = 0.01;
else
    opt.SampTime = con.dT;
end
tic
[results, history, opt2] = staliro(bbmdl,init_cond,input_range,cp_array,phi,preds,TimeHorizon,opt);
runtime=toc;

bestRun = results.optRobIndex;
% hist = history(bestRun).samples;
hist = results.run.bestSample;
%
X0 = [];
falsify_flag = zeros(opt.runs,1);
is_X0_in_cinv = zeros(opt.runs,1);
load('safe_set_ACC_consolidated.mat')
for iii = 1:opt.runs
    falsify_flag(iii) = min(results.run(iii).falsified);
    X0 = [X0 results.run(iii).bestSample(1:3)];
    is_X0_in_cinv(iii) = any(safe_set_ACC_consolidated.contains(X0(:,iii)));
end
1;
%% plotting
if plot_flag
    InpSignals = [];
    if ~isempty(my_x0)
        hist = [0;0;0;hist];
        X0 = my_x0;
    end
    iLength = ceil(TimeHorizon/opt.SampTime/cp_array);
    for i = 1:cp_array-1
        if strcmp(opt.interpolationtype,'linear')
            InpSignals = [InpSignals hist(i+3):(hist(i+4)-hist(i+3))/(iLength-1):hist(i+4)]; %#ok<*AGROW>
        else
            InpSignals = [InpSignals hist(i+3)*ones(1,iLength)]; %#ok<*AGROW>
        end
    end
    InpSignals = InpSignals(1:round(TimeHorizon/opt.SampTime));
    InpSignals = [InpSignals; InpSignals(end)];
%     InpSignals = InpSignals2;
    [T,X,FW,FWS,AL,is_supervised,~] = acc_simulation(...
            X0, supervisor_enabled, controller, 0, plot_flag,...
            k, k_i, mpc_horizon, InpSignals);    

%     figure
%     subplot(3,2,1)
%     hold on
%     plot(T,X(1,:),'b');
%     plot(T,X(3,:),'r');
% 
%     % axis([0 TimeHorizon 0 1.1*max([X(1,:) X2(1,:)])])
%     axis([0 TimeHorizon -1 1.2*max(X(1,:))])
%     legend('vEgo', 'vLead');
%     title('vEgo/vLead')
% 
%     subplot(3,2,3)
%     hold on
%     plot(T,X(2,:),'b');
%     plot([0 TimeHorizon],[con.h_min con.h_min],'k--');
%     axis([0 TimeHorizon 0 1.2*max(X(2,:))])
%     % legend('u', 's');
%     title('dRel')
%     % plot((1:length(X(1,:)))*con.dT,X2(2,:));
% 
% 
%     subplot(3,2,5)
%     hold on
%     plot(T,X(2,:)./(1e-6+X(1,:)), 'b');
%     plot([0 TimeHorizon],[con.tau_min con.tau_min],'k--');
%     % plot((1:length(X(1,:)))*con.dT,con.tau_des*ones(1,length(X(1,:))),'g--');
%     axis([0 TimeHorizon 0 10])
%     % legend('s', 'u');
%     title('Time\_Headway')
% 
%     subplot(3,2,2)
%     hold on
%     plot(T(1:end-1),FW(:), 'b', 'LineWidth',1);
%     plot(T(1:end-1),FWS(:),'--r');
%     plot([0 TimeHorizon],[con.umin_ACC con.umin_ACC],'k--');
%     plot([0 TimeHorizon],[con.umax_ACC con.umax_ACC],'k--');
%     axis([0 TimeHorizon 1.2*con.umin_ACC 1.2*con.umax_ACC])
%     legend('legacy', 'supervised');
%     title('F_w')
% 
% 
%     subplot(3,2,4)
%     hold on
%     stairs(T(1:end-1),is_supervised(:),'b');
%     axis([0 TimeHorizon -0.2 1.2])
%     % legend('u', 's');
%     title('is\_supervised')
% 
%     subplot(3,2,6)
%     hold on
%     plot(T(1:end-1),AL,'b');
%     axis([0 TimeHorizon 1.1*con.al_min 1.1*con.al_max])
%     % legend('u', 's');
%     title('aLead')
    1;
end
