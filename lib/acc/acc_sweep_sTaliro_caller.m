clear all;
load('acc_inverse_sample');
Falsified = [];
for n = 1:10
[falsified, is_interesting] = acc_sweep_sTaliro(...
    acc_inverse_sample, 3, 1600, 800, 0);
Falsified = [Falsified falsified];
end