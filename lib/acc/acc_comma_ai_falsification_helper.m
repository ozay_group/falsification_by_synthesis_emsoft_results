function [T, X, Y, L, CLG, GRD] = acc_comma_ai_falsification_helper(...
    X0, EndTime, TimeStamps, InpSignals)
    global supervisor_enabled my_x0
    global controller
    global k k_i mpc_horizon 
%     global InpSignals2
%     InpSignals2 = InpSignals;
    if ~isempty(my_x0)
        X0 = my_x0;
    end
    ig = 0;
    plot_flag = 0;
    [T,X,~,~,~,~,~] = acc_simulation(...
        X0, supervisor_enabled, controller, ig, plot_flag,...
        k, k_i, mpc_horizon, InpSignals);
    Y = X';
    T = T';
    X = X';
    L = [];
    CLG = [];
    GRD = [];