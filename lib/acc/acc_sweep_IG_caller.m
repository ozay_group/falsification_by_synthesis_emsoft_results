clear all;
load('acc_inverse_sample');
falsified = zeros(size(acc_inverse_sample,1),1);
con = constants_consolidated;
for i = 1:size(acc_inverse_sample,1)
    i
   [~,X] = acc_simulation(acc_inverse_sample(i,:)', 0, 3, 1, 0,1600, 800, 0, []);
     
    if min(X(2,:)) < con.h_min - 1e-6
        falsified(i) = 1;
    end
    if min(X(2,:)./X(1,:)) < con.tau_min - 1e-6
        falsified(i) = 1;
    end

end
1;