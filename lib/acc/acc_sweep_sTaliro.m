function [falsified, is_interesting] = acc_sweep_sTaliro(...
    samples, my_controller, my_k, my_k_i, my_mpc_horizon)
global myx0
falsified = [];
is_interesting = [];
con = constants_consolidated;

for i = 1:100
    myx0 = samples(i*3,:)';
%     if min(myx0(2)) < con.h_min
%         continue
%     end
%     if con.tau_min*myx0(1)-myx0(2) > 0
%         continue
%     end

 [~, falsify_flag, interesting_flag, ~, ~] = acc_comma_ai_falsification(...
    0, my_controller, my_k, my_k_i, my_mpc_horizon, myx0);
    falsified = [falsified; falsify_flag]; %#ok<*AGROW>
    is_interesting = [is_interesting; interesting_flag];
end
1;