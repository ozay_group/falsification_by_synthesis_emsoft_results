function d_t = inverse_game_ACC(x_t, BR_xd, BR_x)
% BR: array of polyhedrons, backwards reachable set
% x0: initial state

% BR_x = BR_xd.projection(1:3); 
% so this outside once and for all to save time

is_inside = BR_x.contains(x_t);
start_idx = find(is_inside,1);
d_t = [];

if isempty(start_idx)
    return;
end

n = length(x_t);    % dim of x
m = BR_xd(1).Dim - n; % dim of u
 
D_win = Polyhedron('A',[BR_xd(start_idx).A],'b',[BR_xd(start_idx).b],...
                   'Ae',[eye(n),zeros(n,m)],'be',x_t); 
               % sent x from BR(i) into BR(i+1)
% u_t = min(U_win.V(:,n+1:end)');
d_t = D_win.V(1,n+1:end)';



