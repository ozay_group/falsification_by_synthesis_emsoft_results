function u_t = controller_MPC_cvxgen(x_t, con, horizon, A,B,E,K)
  con_ACC = con;
   
    % Dynamics: 
    % State & Control Constraints: 
    x_min = [con_ACC.v_min; con_ACC.h_min; con_ACC.vl_min];
    x_max = [con_ACC.v_max; con_ACC.h_max; con_ACC.vl_max];
    u_min = con_ACC.umin_ACC;
    u_max = con_ACC.umax_ACC;

    % Objective: 
    % see output 

    % ===== MIP Formulation ===== 

    % Set solver paramenters
    params_ACC.A = A;
    params_ACC.B = B;
    params_ACC.E = E;
    params_ACC.K = K;

    params_ACC.x_min = x_min;
    params_ACC.x_max = x_max;

    params_ACC.u_min = u_min;
    params_ACC.u_max = u_max;
    params_ACC.d = 0;

    % Solve the MIP: branch and bound + cvxgen solver
    % coder.extrinsic('csolve_mex')

    % Set solver settings + Initialize status;
    settings_ACC.max_iters = 100;
    settings_ACC.verbose = 0;
    settings_ACC.eps = 1e-4;
    settings_ACC.resid_tol = 1e-4;

    status.optval = -Inf;
    status.gap = 0;
    status.steps = 0;
    status.converged = 0;
  
  params_ACC.x0 = x_t;
  params_ACC.target = min(con_ACC.v_des, x_t(2)/con_ACC.tau_des);
  
  u_t = 0;
  if horizon == 2
    [vars, status] = csolve_ACC_mex_2(params_ACC, settings_ACC);
  elseif horizon == 8
    [vars, status] = csolve_ACC_mex_8(params_ACC, settings_ACC);
  elseif horizon == 20
    [vars, status] = csolve_ACC_mex_20(params_ACC, settings_ACC);
  else
      assert(0,'Not implemented');
  end
  % [vars, status] = csolve_ACC_mex_20(params_ACC, settings_ACC);
  u_t = vars.u_0;


