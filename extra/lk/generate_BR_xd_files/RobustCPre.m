function S = RobustCPre(T,X,U,W,  A,B,E,K)
% T: polyhedron, target set
% X: polyhedron, domain
% U: polyhedron, control action set
% W: polyhedron, disturbance set
% discrete time dynamics: x+ = Ax + Bu + Ew + K

Nw = size(W.V,1);
S = Polyhedron('A',[],'b',[]);
for nw = 1:1:Nw
    A_nw = [T.A*A, T.A*B]; 
    b_nw = [T.b - T.A*E*W.V(nw,:)' - T.A*K];
    S = Polyhedron('A',[S.A; A_nw],'b',[S.b; b_nw]);
end

% u\in U
S = Polyhedron('A',[S.A; [zeros(size(U.A,1),T.Dim), U.A]],'b',[S.b; U.b]);
% x\in X
% S = Polyhedron('A',[S.A; [X.A, zeros(size(X.A,1),U.Dim)]],'b',[S.b; X.b]);







