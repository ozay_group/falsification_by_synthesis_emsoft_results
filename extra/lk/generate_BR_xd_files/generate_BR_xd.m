clear,clc;
load('CInv_LK');
% get dynamics, sets
con = constants_consolidated;
Dt = 0.01;
[A,B,E,K, X,U,W] = getABEK_XUW_LK(con, Dt);

% safe set
safex_max = [0.9; con.x_max(2:4)];
safex_min = -safex_max;
S = Polyhedron('lb',safex_min,...
                   'ub',safex_max); 

% target sets (i.e., unsafe)
T_list = {};
for dim = 1:1:4
    H = zeros(1,4);
    H(dim) = 1;
    T_list{end+1} = Polyhedron('A',H,'b',safex_min(dim));
    T_list{end+1} = Polyhedron('A',-H,'b',-safex_max(dim));
end

prjct_dim = [1 2 3];    
BR_xw = [];
for idx = 1:1:length(T_list)
    T = T_list{idx};
    hold on
    S_xw = RobustCPre(T,X,W,U,  A,E,B,K);
    BR_xw = [BR_xw, S_xw];
    hold on
    for iter = 1:1:50
        iter
        T = S_xw.projection(1:4);
        S_xw = RobustCPre(T,X,W,U,  A,E,B,K);
        if isEmptySet(intersect(projection(S_xw,1:4,'fourier'), S))
            break;
        else
            BR_xw = [BR_xw, S_xw];
        end
        hold on
        plot(projection(intersect(S_xw.projection(1:4,'ifourier'),X),prjct_dim,'ifourier'),'color','blue','edgecolor', 'blue','alpha', 0.2, 'linestyle', 'none');
    end
end
BR_xd = BR_xw;
plot(projection(CInv_LK, prjct_dim, 'ifourier'),'color','red','edgecolor', 'blue','alpha', 0.2, 'linestyle', 'none');





