function u = extractStrategy(x0, BR_xu)
% BR: array of polyhedrons, backwards reachable set
% x0: initial state

n = length(x0);    % dim of x
m = BR_xu(1).Dim - n; % dim of u

BR_x = BR_xu.projection(1:n);

is_inside = BR_x.contains(x0);
start_idx = find(is_inside,1);
u = [];

if isempty(start_idx)
    return;
end
 
U_win = Polyhedron('A',[BR_xu(start_idx).A],'b',[BR_xu(start_idx).b],...
                   'Ae',[eye(n),zeros(n,m)],'be',x0); 
               % sent x from BR(i) into BR(i+1)
u = U_win.V(1,n+1:end)';



