function Samples = SampleBoundaryUniform(safe_set_A, safe_set_b)
% Return samples on the boundary of 4D safe_set (FOR LANE KEEPING)
% Input: safe_set, one Polyhedron;
% Output: samples: M x 3 matrix, 
%     M is the number of samples, 
% Note: sample is done by brute force (i.e., by definition of boundary)
%     The returned points lie strctly inside the safe_set.

% Slow
% xmin = min(safe_set.V,[],2);
% xmax = max(safe_set.V,[],2);
car_con = constants_consolidated();

% con.y_max = 1.8;
% con.nu_max = 1;            % max lateral speed [m/s]
% % con.psi_max = deg2rad(45);  % max yaw angle     [deg]
% % con.r_max = deg2rad(45);    % max yaw rate      [deg/s]
% con.psi_max = 0.15;  % max yaw angle     [deg]
% % con.r_max = 0.399876967583812;    % max yaw rate      [deg/s]
% % con.r_max = 0.365059553531220;    % max yaw rate      [deg/s]
% con.r_max = car_con.a*car_con.u0*car_con.F_yfmax/(car_con.b^2*car_con.Car);

% xmin = -[con.y_max; con.nu_max; con.psi_max; con.r_max];
xmin = car_con.x_min;
% xmax = [con.y_max; con.nu_max; con.psi_max; con.r_max];
xmax = car_con.x_max;

% n1 = 20;
% n2 = 20;
% n3 = 20;
% n4 = 10;

Samples = [];
% test_pts = rand(xmax(1) - xmin(1), xmax(2) - xmin(2), xmax(3) - xmin(3), xmax(4) - xmin(4));
test_pts = inf(1000000, 4);
test_pts(:, 1) = (xmax(1) - xmin(1))*rand([1000000, 1]) - (xmax(1) - xmin(1))/2;
test_pts(:, 2) = (xmax(2) - xmin(2))*rand([1000000, 1]) - (xmax(2) - xmin(2))/2;
test_pts(:, 3) = (xmax(3) - xmin(3))*rand([1000000, 1]) - (xmax(3) - xmin(3))/2;
test_pts(:, 4) = (xmax(4) - xmin(4))*rand([1000000, 1]) - (xmax(4) - xmin(4))/2;

% X1 = linspace(xmin(1),xmax(1),n1);
% X2 = linspace(xmin(2),xmax(2),n2);
% X3 = linspace(xmin(3),xmax(3),n3);
% X4 = linspace(xmin(4),xmax(4),n4);
% rad = [X1(2)-X1(1);X2(2)-X2(1);X3(2)-X3(1);X4(2)-X4(1)]/2;
% ct = 0;
for i = 1:length(test_pts)
    x = test_pts(i, :);
    if safe_set_A*x' <= safe_set_b
%       ct = ct+1
        nearby_pts = inf(50, 4);
        nearby_pts(:, 1) = 0.001*rand([50, 1]) - 0.0005 + x(1);
        nearby_pts(:, 2) = 0.001*rand([50, 1]) - 0.0005 + x(2);
        nearby_pts(:, 3) = 0.001*rand([50, 1]) - 0.0005 + x(3);
        nearby_pts(:, 4) = 0.001*rand([50, 1]) - 0.0005 + x(4);
%         Bx = Polyhedron('ub',x'+0.001*ones(4,1), 'lb', x'-0.001*ones(4,1));
%         if ~all(all(safe_set_A*Bx.V' <= repmat(safe_set_b, 1, 16)))
        if ~all(all(safe_set_A*nearby_pts' <= repmat(safe_set_b, 1, 50)))
          Samples = [Samples; x];
%           hold on
%           plot3(sample(1),sample(2),sample(3),'b*','Linewidth',4);
        end
    end
end




