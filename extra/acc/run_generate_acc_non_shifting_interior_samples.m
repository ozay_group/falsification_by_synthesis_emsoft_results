% This script generates 275 samples point in the interior of cinv of ACC
% These data, however, are not used in any experiments in the EMSOFT paper
% these points are selected to be uniformly inside the cinv, and to 
% verify conjecture that "the boundary is harder than the interior" 
% (although, as explained in the paper, this is not always the case) 
% the number of samples is picked to be 275, simply to match the number of 
% the boundary samples;
% ALSO see Read_Me

clear,clc

load('safe_set_ACC_consolidated');
safe_set = safe_set_ACC_consolidated; 

acc_uniform_interior_sample_275 = [];
norm_vec = [];
for x1 = 0:2:25
    for x2 = 0:10:120
        for x3 = 0:1.25:25
            x = [x1, x2, x3];
            % if any(safe_set_ACC_consolidated.contains(x'))
            if ~any(safe_set_ACC_consolidated.contains(x')) && x2-1 >= 4 && x1 <= (x2-1)/1.7 
                % acc_uniform_interior_sample_275 = [acc_uniform_interior_sample_275; x + [0, 5, 0]];
                acc_uniform_interior_sample_275 = [acc_uniform_interior_sample_275; x+ [0, -1, 0]];
                norm_vec = [norm_vec; norm((x - [0, 130, 20])./[25, 120, 20],2)];
            end
        end
    end
end

% [~,Idx] = sort(norm_vec); % small -> large
% acc_uniform_interior_sample_275 = acc_uniform_interior_sample_275(Idx(end:-1:end-274), :);
scatter3(acc_uniform_interior_sample_275(:,1),acc_uniform_interior_sample_275(:,2),acc_uniform_interior_sample_275(:,3),'r')
