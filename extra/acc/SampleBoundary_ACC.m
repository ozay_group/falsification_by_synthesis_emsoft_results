function Samples = SampleBoundary_ACC(safe_set)
% Return samples on the boundary of union of 3D safe_set, the samples are 
% excluded if they lie on the domain.
% Input: 
%     safe_set, array of Polyhedron;
%     con, constant file, used to determine the domain
% Output: 
%     Samples: M x 3 matrix, 
%     M is the number of samples, 
%     each sample is in order [v, h, v_L];

% we pick every one of the three polytopes from the sequence
% just to skip the "intermediate" polytopes
safe_set = safe_set(1:3:end);

% Note the following domain upper/lower bounds are DIFFERENT from the ones 
% in "constant_consolidated" files, but this is fine, because: 
%  1) there are some filtering in test.m that removes the points outside 
%     the ACC controlled invariant set "safe_set_ACC_consolidated";
%  2) see line 34 of this function, the extra domain is added by "21:1:25",
%     which corresponds to the extra 70 points 
xmin = [0,0,0];
xmax = [35, 200, 20];

n1 = 20;
n3 = 20;

Samples = [];
N = length(safe_set);

j = 1;
for x1 = linspace(xmin(1),xmax(1),n1)
    for x3 = [linspace(xmin(3),xmax(3),n3),21:1:25]
        % j = j+1
        x2 = Inf;
        for i = 1:1:N
            P_temp = Polyhedron('A', safe_set(i).A, ...
                                'b', safe_set(i).b, ...
                                'Ae', [1 0 0; 0 0 1], ...
                                'be', [x1;x3]) ;
            if ~isempty(P_temp.V(:,2))
                x2 = min([x2, min(P_temp.V(:,2))]);
                if x2 > xmax(2)-1e-2 || x2 < xmin(2)+1e-2
                    break;
                end
            end
        end
        sample = [x1,x2,x3];
        if x2 < xmax(2)-1e-2 && x2 > xmin(2)+1e-2
            Samples = [Samples; sample];
        end
    end
end





