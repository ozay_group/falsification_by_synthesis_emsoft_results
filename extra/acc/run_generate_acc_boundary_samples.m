% This script generates 345 samples point on the boundary of cinv of ACC
% the 345 points generated matches with the 275 + extra 70 points used in
% the experiement;
% ALSO see "Read_Me.txt"

clear,clc

load('safe_set_ACC_consolidated');
CInv_ACC = safe_set_ACC_consolidated; 

acc_boundary0 = SampleBoundary_ACC(CInv_ACC);

% remove points outside cinv
N = length(acc_boundary0);
idx1 = [];
for i = 1:1:N
    if ~CInv_ACC.contains(acc_boundary0(i,:)')
        idx1(end+1) = i;
    end
end
acc_boundary0(idx1,:) = [];
 


