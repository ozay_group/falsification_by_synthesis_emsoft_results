% csolve  Solves a custom quadratic program very rapidly.
%
% [vars, status] = csolve(params, settings)
%
% solves the convex optimization problem
%
%   minimize(abs(x_1(1) - target) + abs(x_2(1) - target) + abs(x_3(1) - target) + abs(x_4(1) - target) + abs(x_5(1) - target) + abs(x_6(1) - target) + abs(x_7(1) - target) + abs(x_8(1) - target))
%   subject to
%     x_1 - (A*x_0 + B*u_0 + d*E + K) == 0
%     x_2 - (A*x_1 + B*u_1 + d*E + K) == 0
%     x_3 - (A*x_2 + B*u_2 + d*E + K) == 0
%     x_4 - (A*x_3 + B*u_3 + d*E + K) == 0
%     x_5 - (A*x_4 + B*u_4 + d*E + K) == 0
%     x_6 - (A*x_5 + B*u_5 + d*E + K) == 0
%     x_7 - (A*x_6 + B*u_6 + d*E + K) == 0
%     x_8 - (A*x_7 + B*u_7 + d*E + K) == 0
%     x_0 == x0
%     x_min <= x_1
%     x_min <= x_2
%     x_min <= x_3
%     x_min <= x_4
%     x_min <= x_5
%     x_min <= x_6
%     x_min <= x_7
%     x_1 <= x_max
%     x_2 <= x_max
%     x_3 <= x_max
%     x_4 <= x_max
%     x_5 <= x_max
%     x_6 <= x_max
%     x_7 <= x_max
%     u_min <= u_0
%     u_min <= u_1
%     u_min <= u_2
%     u_min <= u_3
%     u_min <= u_4
%     u_min <= u_5
%     u_min <= u_6
%     u_min <= u_7
%     u_0 <= u_max
%     u_1 <= u_max
%     u_2 <= u_max
%     u_3 <= u_max
%     u_4 <= u_max
%     u_5 <= u_max
%     u_6 <= u_max
%     u_7 <= u_max
%
% with variables
%      u_0   1 x 1
%      u_1   1 x 1
%      u_2   1 x 1
%      u_3   1 x 1
%      u_4   1 x 1
%      u_5   1 x 1
%      u_6   1 x 1
%      u_7   1 x 1
%      x_0   3 x 1
%      x_1   3 x 1
%      x_2   3 x 1
%      x_3   3 x 1
%      x_4   3 x 1
%      x_5   3 x 1
%      x_6   3 x 1
%      x_7   3 x 1
%      x_8   3 x 1
%
% and parameters
%        A   3 x 3
%        B   3 x 1
%        E   3 x 1
%        K   3 x 1
%        d   1 x 1
%   target   1 x 1
%    u_max   1 x 1
%    u_min   1 x 1
%       x0   3 x 1
%    x_max   3 x 1
%    x_min   3 x 1
%
% Note:
%   - Check status.converged, which will be 1 if optimization succeeded.
%   - You don't have to specify settings if you don't want to.
%   - To hide output, use settings.verbose = 0.
%   - To change iterations, use settings.max_iters = 20.
%   - You may wish to compare with cvxsolve to check the solver is correct.
%
% Specify params.A, ..., params.x_min, then run
%   [vars, status] = csolve(params, settings)
% Produced by CVXGEN, 2018-01-29 15:58:05 -0500.
% CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com.
% The code in this file is Copyright (C) 2006-2017 Jacob Mattingley.
% CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
% applications without prior written permission from Jacob Mattingley.

% Filename: csolve.m.
% Description: Help file for the Matlab solver interface.
