% Produced by CVXGEN, 2018-01-29 14:34:36 -0500.
% CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com.
% The code in this file is Copyright (C) 2006-2017 Jacob Mattingley.
% CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
% applications without prior written permission from Jacob Mattingley.

% Filename: cvxsolve.m.
% Description: Solution file, via cvx, for use with sample.m.
function [vars, status] = cvxsolve(params, settings)
A = params.A;
B = params.B;
E = params.E;
K = params.K;
d = params.d;
target = params.target;
u_max = params.u_max;
u_min = params.u_min;
x0 = params.x0;
x_max = params.x_max;
x_min = params.x_min;
cvx_begin
  % Caution: automatically generated by cvxgen. May be incorrect.
  variable x_1(3, 1);
  variable x_2(3, 1);
  variable x_3(3, 1);
  variable x_4(3, 1);
  variable x_5(3, 1);
  variable x_6(3, 1);
  variable x_7(3, 1);
  variable x_8(3, 1);
  variable x_9(3, 1);
  variable x_10(3, 1);
  variable x_11(3, 1);
  variable x_12(3, 1);
  variable x_13(3, 1);
  variable x_14(3, 1);
  variable x_15(3, 1);
  variable x_16(3, 1);
  variable x_17(3, 1);
  variable x_18(3, 1);
  variable x_19(3, 1);
  variable x_20(3, 1);
  variable x_0(3, 1);
  variable u_0;
  variable u_1;
  variable u_2;
  variable u_3;
  variable u_4;
  variable u_5;
  variable u_6;
  variable u_7;
  variable u_8;
  variable u_9;
  variable u_10;
  variable u_11;
  variable u_12;
  variable u_13;
  variable u_14;
  variable u_15;
  variable u_16;
  variable u_17;
  variable u_18;
  variable u_19;

  minimize(abs(x_1(1) - target) + abs(x_2(1) - target) + abs(x_3(1) - target) + abs(x_4(1) - target) + abs(x_5(1) - target) + abs(x_6(1) - target) + abs(x_7(1) - target) + abs(x_8(1) - target) + abs(x_9(1) - target) + abs(x_10(1) - target) + abs(x_11(1) - target) + abs(x_12(1) - target) + abs(x_13(1) - target) + abs(x_14(1) - target) + abs(x_15(1) - target) + abs(x_16(1) - target) + abs(x_17(1) - target) + abs(x_18(1) - target) + abs(x_19(1) - target) + abs(x_20(1) - target));
  subject to
    x_1 - (A*x_0 + B*u_0 + d*E + K) == 0;
    x_2 - (A*x_1 + B*u_1 + d*E + K) == 0;
    x_3 - (A*x_2 + B*u_2 + d*E + K) == 0;
    x_4 - (A*x_3 + B*u_3 + d*E + K) == 0;
    x_5 - (A*x_4 + B*u_4 + d*E + K) == 0;
    x_6 - (A*x_5 + B*u_5 + d*E + K) == 0;
    x_7 - (A*x_6 + B*u_6 + d*E + K) == 0;
    x_8 - (A*x_7 + B*u_7 + d*E + K) == 0;
    x_9 - (A*x_8 + B*u_8 + d*E + K) == 0;
    x_10 - (A*x_9 + B*u_9 + d*E + K) == 0;
    x_11 - (A*x_10 + B*u_10 + d*E + K) == 0;
    x_12 - (A*x_11 + B*u_11 + d*E + K) == 0;
    x_13 - (A*x_12 + B*u_12 + d*E + K) == 0;
    x_14 - (A*x_13 + B*u_13 + d*E + K) == 0;
    x_15 - (A*x_14 + B*u_14 + d*E + K) == 0;
    x_16 - (A*x_15 + B*u_15 + d*E + K) == 0;
    x_17 - (A*x_16 + B*u_16 + d*E + K) == 0;
    x_18 - (A*x_17 + B*u_17 + d*E + K) == 0;
    x_19 - (A*x_18 + B*u_18 + d*E + K) == 0;
    x_20 - (A*x_19 + B*u_19 + d*E + K) == 0;
    x_0 == x0;
    x_min <= x_1;
    x_min <= x_2;
    x_min <= x_3;
    x_min <= x_4;
    x_min <= x_5;
    x_min <= x_6;
    x_min <= x_7;
    x_min <= x_8;
    x_min <= x_9;
    x_min <= x_10;
    x_min <= x_11;
    x_min <= x_12;
    x_min <= x_13;
    x_min <= x_14;
    x_min <= x_15;
    x_min <= x_16;
    x_min <= x_17;
    x_min <= x_18;
    x_min <= x_19;
    x_1 <= x_max;
    x_2 <= x_max;
    x_3 <= x_max;
    x_4 <= x_max;
    x_5 <= x_max;
    x_6 <= x_max;
    x_7 <= x_max;
    x_8 <= x_max;
    x_9 <= x_max;
    x_10 <= x_max;
    x_11 <= x_max;
    x_12 <= x_max;
    x_13 <= x_max;
    x_14 <= x_max;
    x_15 <= x_max;
    x_16 <= x_max;
    x_17 <= x_max;
    x_18 <= x_max;
    x_19 <= x_max;
    u_min <= u_0;
    u_min <= u_1;
    u_min <= u_2;
    u_min <= u_3;
    u_min <= u_4;
    u_min <= u_5;
    u_min <= u_6;
    u_min <= u_7;
    u_min <= u_8;
    u_min <= u_9;
    u_min <= u_10;
    u_min <= u_11;
    u_min <= u_12;
    u_min <= u_13;
    u_min <= u_14;
    u_min <= u_15;
    u_min <= u_16;
    u_min <= u_17;
    u_min <= u_18;
    u_min <= u_19;
    u_0 <= u_max;
    u_1 <= u_max;
    u_2 <= u_max;
    u_3 <= u_max;
    u_4 <= u_max;
    u_5 <= u_max;
    u_6 <= u_max;
    u_7 <= u_max;
    u_8 <= u_max;
    u_9 <= u_max;
    u_10 <= u_max;
    u_11 <= u_max;
    u_12 <= u_max;
    u_13 <= u_max;
    u_14 <= u_max;
    u_15 <= u_max;
    u_16 <= u_max;
    u_17 <= u_max;
    u_18 <= u_max;
    u_19 <= u_max;
cvx_end
vars.u_0 = u_0;
vars.u_1 = u_1;
vars.u{1} = u_1;
vars.u_2 = u_2;
vars.u{2} = u_2;
vars.u_3 = u_3;
vars.u{3} = u_3;
vars.u_4 = u_4;
vars.u{4} = u_4;
vars.u_5 = u_5;
vars.u{5} = u_5;
vars.u_6 = u_6;
vars.u{6} = u_6;
vars.u_7 = u_7;
vars.u{7} = u_7;
vars.u_8 = u_8;
vars.u{8} = u_8;
vars.u_9 = u_9;
vars.u{9} = u_9;
vars.u_10 = u_10;
vars.u{10} = u_10;
vars.u_11 = u_11;
vars.u{11} = u_11;
vars.u_12 = u_12;
vars.u{12} = u_12;
vars.u_13 = u_13;
vars.u{13} = u_13;
vars.u_14 = u_14;
vars.u{14} = u_14;
vars.u_15 = u_15;
vars.u{15} = u_15;
vars.u_16 = u_16;
vars.u{16} = u_16;
vars.u_17 = u_17;
vars.u{17} = u_17;
vars.u_18 = u_18;
vars.u{18} = u_18;
vars.u_19 = u_19;
vars.u{19} = u_19;
vars.x_0 = x_0;
vars.x_1 = x_1;
vars.x{1} = x_1;
vars.x_2 = x_2;
vars.x{2} = x_2;
vars.x_3 = x_3;
vars.x{3} = x_3;
vars.x_4 = x_4;
vars.x{4} = x_4;
vars.x_5 = x_5;
vars.x{5} = x_5;
vars.x_6 = x_6;
vars.x{6} = x_6;
vars.x_7 = x_7;
vars.x{7} = x_7;
vars.x_8 = x_8;
vars.x{8} = x_8;
vars.x_9 = x_9;
vars.x{9} = x_9;
vars.x_10 = x_10;
vars.x{10} = x_10;
vars.x_11 = x_11;
vars.x{11} = x_11;
vars.x_12 = x_12;
vars.x{12} = x_12;
vars.x_13 = x_13;
vars.x{13} = x_13;
vars.x_14 = x_14;
vars.x{14} = x_14;
vars.x_15 = x_15;
vars.x{15} = x_15;
vars.x_16 = x_16;
vars.x{16} = x_16;
vars.x_17 = x_17;
vars.x{17} = x_17;
vars.x_18 = x_18;
vars.x{18} = x_18;
vars.x_19 = x_19;
vars.x{19} = x_19;
vars.x_20 = x_20;
vars.x{20} = x_20;
status.cvx_status = cvx_status;
% Provide a drop-in replacement for csolve.
status.optval = cvx_optval;
status.converged = strcmp(cvx_status, 'Solved');
