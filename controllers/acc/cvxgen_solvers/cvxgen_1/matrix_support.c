/* Produced by CVXGEN, 2018-03-29 13:37:15 -0400.  */
/* CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2017 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: matrix_support.c. */
/* Description: Support functions for matrix multiplication and vector filling. */
#include "solver.h"
void multbymA(double *lhs, double *rhs) {
  lhs[0] = -rhs[1]*(-params.B[0])-rhs[2]*(-params.A[0])-rhs[3]*(-params.A[3])-rhs[4]*(-params.A[6])-rhs[5]*(1);
  lhs[1] = -rhs[1]*(-params.B[1])-rhs[2]*(-params.A[1])-rhs[3]*(-params.A[4])-rhs[4]*(-params.A[7])-rhs[6]*(1);
  lhs[2] = -rhs[1]*(-params.B[2])-rhs[2]*(-params.A[2])-rhs[3]*(-params.A[5])-rhs[4]*(-params.A[8])-rhs[7]*(1);
  lhs[3] = -rhs[2]*(1);
  lhs[4] = -rhs[3]*(1);
  lhs[5] = -rhs[4]*(1);
}
void multbymAT(double *lhs, double *rhs) {
  lhs[0] = 0;
  lhs[1] = -rhs[0]*(-params.B[0])-rhs[1]*(-params.B[1])-rhs[2]*(-params.B[2]);
  lhs[2] = -rhs[0]*(-params.A[0])-rhs[1]*(-params.A[1])-rhs[2]*(-params.A[2])-rhs[3]*(1);
  lhs[3] = -rhs[0]*(-params.A[3])-rhs[1]*(-params.A[4])-rhs[2]*(-params.A[5])-rhs[4]*(1);
  lhs[4] = -rhs[0]*(-params.A[6])-rhs[1]*(-params.A[7])-rhs[2]*(-params.A[8])-rhs[5]*(1);
  lhs[5] = -rhs[0]*(1);
  lhs[6] = -rhs[1]*(1);
  lhs[7] = -rhs[2]*(1);
}
void multbymG(double *lhs, double *rhs) {
  lhs[0] = -rhs[0]*(-1)-rhs[5]*(1);
  lhs[1] = -rhs[0]*(-1)-rhs[5]*(-1);
  lhs[2] = -rhs[1]*(-1);
  lhs[3] = -rhs[1]*(1);
}
void multbymGT(double *lhs, double *rhs) {
  lhs[0] = -rhs[0]*(-1)-rhs[1]*(-1);
  lhs[1] = -rhs[2]*(-1)-rhs[3]*(1);
  lhs[2] = 0;
  lhs[3] = 0;
  lhs[4] = 0;
  lhs[5] = -rhs[0]*(1)-rhs[1]*(-1);
  lhs[6] = 0;
  lhs[7] = 0;
}
void multbyP(double *lhs, double *rhs) {
  /* TODO use the fact that P is symmetric? */
  /* TODO check doubling / half factor etc. */
  lhs[0] = 0;
  lhs[1] = 0;
  lhs[2] = 0;
  lhs[3] = 0;
  lhs[4] = 0;
  lhs[5] = 0;
  lhs[6] = 0;
  lhs[7] = 0;
}
void fillq(void) {
  work.q[0] = 1;
  work.q[1] = 0;
  work.q[2] = 0;
  work.q[3] = 0;
  work.q[4] = 0;
  work.q[5] = 0;
  work.q[6] = 0;
  work.q[7] = 0;
}
void fillh(void) {
  work.h[0] = params.target[0];
  work.h[1] = -params.target[0];
  work.h[2] = -params.u_min[0];
  work.h[3] = params.u_max[0];
}
void fillb(void) {
  work.b[0] = params.d[0]*params.E[0]+params.K[0];
  work.b[1] = params.d[0]*params.E[1]+params.K[1];
  work.b[2] = params.d[0]*params.E[2]+params.K[2];
  work.b[3] = params.x0[0];
  work.b[4] = params.x0[1];
  work.b[5] = params.x0[2];
}
void pre_ops(void) {
}
