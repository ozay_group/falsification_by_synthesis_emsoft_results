% csolve  Solves a custom quadratic program very rapidly.
%
% [vars, status] = csolve(params, settings)
%
% solves the convex optimization problem
%
%   minimize(abs(x_1(1) - target))
%   subject to
%     x_1 - (A*x_0 + B*u_0 + d*E + K) == 0
%     x_0 == x0
%     u_min <= u_0
%     u_0 <= u_max
%
% with variables
%      u_0   1 x 1
%      x_0   3 x 1
%      x_1   3 x 1
%
% and parameters
%        A   3 x 3
%        B   3 x 1
%        E   3 x 1
%        K   3 x 1
%        d   1 x 1
%   target   1 x 1
%    u_max   1 x 1
%    u_min   1 x 1
%       x0   3 x 1
%
% Note:
%   - Check status.converged, which will be 1 if optimization succeeded.
%   - You don't have to specify settings if you don't want to.
%   - To hide output, use settings.verbose = 0.
%   - To change iterations, use settings.max_iters = 20.
%   - You may wish to compare with cvxsolve to check the solver is correct.
%
% Specify params.A, ..., params.x0, then run
%   [vars, status] = csolve(params, settings)
% Produced by CVXGEN, 2018-03-29 13:37:15 -0400.
% CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com.
% The code in this file is Copyright (C) 2006-2017 Jacob Mattingley.
% CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
% applications without prior written permission from Jacob Mattingley.

% Filename: csolve.m.
% Description: Help file for the Matlab solver interface.
