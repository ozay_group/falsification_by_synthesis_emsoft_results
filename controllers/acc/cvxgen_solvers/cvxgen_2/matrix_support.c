/* Produced by CVXGEN, 2018-03-29 13:33:44 -0400.  */
/* CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2017 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: matrix_support.c. */
/* Description: Support functions for matrix multiplication and vector filling. */
#include "solver.h"
void multbymA(double *lhs, double *rhs) {
  lhs[0] = -rhs[2]*(-params.B[0])-rhs[4]*(-params.A[0])-rhs[5]*(-params.A[3])-rhs[6]*(-params.A[6])-rhs[7]*(1);
  lhs[1] = -rhs[2]*(-params.B[1])-rhs[4]*(-params.A[1])-rhs[5]*(-params.A[4])-rhs[6]*(-params.A[7])-rhs[8]*(1);
  lhs[2] = -rhs[2]*(-params.B[2])-rhs[4]*(-params.A[2])-rhs[5]*(-params.A[5])-rhs[6]*(-params.A[8])-rhs[9]*(1);
  lhs[3] = -rhs[3]*(-params.B[0])-rhs[7]*(-params.A[0])-rhs[8]*(-params.A[3])-rhs[9]*(-params.A[6])-rhs[10]*(1);
  lhs[4] = -rhs[3]*(-params.B[1])-rhs[7]*(-params.A[1])-rhs[8]*(-params.A[4])-rhs[9]*(-params.A[7])-rhs[11]*(1);
  lhs[5] = -rhs[3]*(-params.B[2])-rhs[7]*(-params.A[2])-rhs[8]*(-params.A[5])-rhs[9]*(-params.A[8])-rhs[12]*(1);
  lhs[6] = -rhs[4]*(1);
  lhs[7] = -rhs[5]*(1);
  lhs[8] = -rhs[6]*(1);
}
void multbymAT(double *lhs, double *rhs) {
  lhs[0] = 0;
  lhs[1] = 0;
  lhs[2] = -rhs[0]*(-params.B[0])-rhs[1]*(-params.B[1])-rhs[2]*(-params.B[2]);
  lhs[3] = -rhs[3]*(-params.B[0])-rhs[4]*(-params.B[1])-rhs[5]*(-params.B[2]);
  lhs[4] = -rhs[0]*(-params.A[0])-rhs[1]*(-params.A[1])-rhs[2]*(-params.A[2])-rhs[6]*(1);
  lhs[5] = -rhs[0]*(-params.A[3])-rhs[1]*(-params.A[4])-rhs[2]*(-params.A[5])-rhs[7]*(1);
  lhs[6] = -rhs[0]*(-params.A[6])-rhs[1]*(-params.A[7])-rhs[2]*(-params.A[8])-rhs[8]*(1);
  lhs[7] = -rhs[0]*(1)-rhs[3]*(-params.A[0])-rhs[4]*(-params.A[1])-rhs[5]*(-params.A[2]);
  lhs[8] = -rhs[1]*(1)-rhs[3]*(-params.A[3])-rhs[4]*(-params.A[4])-rhs[5]*(-params.A[5]);
  lhs[9] = -rhs[2]*(1)-rhs[3]*(-params.A[6])-rhs[4]*(-params.A[7])-rhs[5]*(-params.A[8]);
  lhs[10] = -rhs[3]*(1);
  lhs[11] = -rhs[4]*(1);
  lhs[12] = -rhs[5]*(1);
}
void multbymG(double *lhs, double *rhs) {
  lhs[0] = -rhs[0]*(-1)-rhs[7]*(1);
  lhs[1] = -rhs[0]*(-1)-rhs[7]*(-1);
  lhs[2] = -rhs[1]*(-1)-rhs[10]*(1);
  lhs[3] = -rhs[1]*(-1)-rhs[10]*(-1);
  lhs[4] = -rhs[7]*(-1);
  lhs[5] = -rhs[8]*(-1);
  lhs[6] = -rhs[9]*(-1);
  lhs[7] = -rhs[7]*(1);
  lhs[8] = -rhs[8]*(1);
  lhs[9] = -rhs[9]*(1);
  lhs[10] = -rhs[2]*(-1);
  lhs[11] = -rhs[3]*(-1);
  lhs[12] = -rhs[2]*(1);
  lhs[13] = -rhs[3]*(1);
}
void multbymGT(double *lhs, double *rhs) {
  lhs[0] = -rhs[0]*(-1)-rhs[1]*(-1);
  lhs[1] = -rhs[2]*(-1)-rhs[3]*(-1);
  lhs[2] = -rhs[10]*(-1)-rhs[12]*(1);
  lhs[3] = -rhs[11]*(-1)-rhs[13]*(1);
  lhs[4] = 0;
  lhs[5] = 0;
  lhs[6] = 0;
  lhs[7] = -rhs[0]*(1)-rhs[1]*(-1)-rhs[4]*(-1)-rhs[7]*(1);
  lhs[8] = -rhs[5]*(-1)-rhs[8]*(1);
  lhs[9] = -rhs[6]*(-1)-rhs[9]*(1);
  lhs[10] = -rhs[2]*(1)-rhs[3]*(-1);
  lhs[11] = 0;
  lhs[12] = 0;
}
void multbyP(double *lhs, double *rhs) {
  /* TODO use the fact that P is symmetric? */
  /* TODO check doubling / half factor etc. */
  lhs[0] = 0;
  lhs[1] = 0;
  lhs[2] = 0;
  lhs[3] = 0;
  lhs[4] = 0;
  lhs[5] = 0;
  lhs[6] = 0;
  lhs[7] = 0;
  lhs[8] = 0;
  lhs[9] = 0;
  lhs[10] = 0;
  lhs[11] = 0;
  lhs[12] = 0;
}
void fillq(void) {
  work.q[0] = 1;
  work.q[1] = 1;
  work.q[2] = 0;
  work.q[3] = 0;
  work.q[4] = 0;
  work.q[5] = 0;
  work.q[6] = 0;
  work.q[7] = 0;
  work.q[8] = 0;
  work.q[9] = 0;
  work.q[10] = 0;
  work.q[11] = 0;
  work.q[12] = 0;
}
void fillh(void) {
  work.h[0] = params.target[0];
  work.h[1] = -params.target[0];
  work.h[2] = params.target[0];
  work.h[3] = -params.target[0];
  work.h[4] = -params.x_min[0];
  work.h[5] = -params.x_min[1];
  work.h[6] = -params.x_min[2];
  work.h[7] = params.x_max[0];
  work.h[8] = params.x_max[1];
  work.h[9] = params.x_max[2];
  work.h[10] = -params.u_min[0];
  work.h[11] = -params.u_min[0];
  work.h[12] = params.u_max[0];
  work.h[13] = params.u_max[0];
}
void fillb(void) {
  work.b[0] = params.d[0]*params.E[0]+params.K[0];
  work.b[1] = params.d[0]*params.E[1]+params.K[1];
  work.b[2] = params.d[0]*params.E[2]+params.K[2];
  work.b[3] = params.d[0]*params.E[0]+params.K[0];
  work.b[4] = params.d[0]*params.E[1]+params.K[1];
  work.b[5] = params.d[0]*params.E[2]+params.K[2];
  work.b[6] = params.x0[0];
  work.b[7] = params.x0[1];
  work.b[8] = params.x0[2];
}
void pre_ops(void) {
}
