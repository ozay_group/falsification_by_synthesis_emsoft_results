import sys
import numpy as np

sys.path.append('../')

import longcontrol as LoC
import adaptivecruise as AC
import pathplanner as PP
import latcontrol as LaC
import vehicle_model as VM
from drive_helpers import learn_angle_offset

class Car(object):

  def __init__(self, vLead, dRel, vRel, aLeadK):
    self.vLead = vLead
    self.dRel = dRel
    self.vRel = vRel
    self.aLeadK = aLeadK

class Controller(object):

  def __init__(self, v_ego, v_ego_des):
    self.acc = AC.AdaptiveCruise()
    self.pid_c = LoC.LongControl()
    self.pid_c.reset(v_ego)
    self.v_ego_des_kph = v_ego_des * 3.6;

  def update(self, enabled, vEgo, vLead1, dRel1, aLeadK1):
    vRel1 = vLead1 - vEgo
    vRel2 = vLead1 - vEgo
    l1 = Car(vLead1, dRel1, vRel1, aLeadK1)
    self.acc.update(vEgo, self.pid_c.v_pid, l1)
    gas, brake = self.pid_c.update(enabled, vEgo, self.v_ego_des_kph,
                                   self.acc.v_target_lead, self.acc.a_target, 
                                   self.acc.jerk_factor, True)
    return gas, brake