function u_t = controller_MPC_LK(x_t, d_t, con, Dt, horizon)

[A,B,E,K, ~,~,~] = getABEK_XUW_LK(con, Dt);

x_min = con.x_min;
x_max = con.x_max;
u_min = con.u_min_LK;
u_max = con.u_max_LK;

% Objective: 
Q = diag([1 0 0 0]);                     
R = 0;                              

% ===== MIP Formulation ===== 

% Set solver paramenters
params.A = A;
params.B = B;
params.E = E;
params.K = K;

params.x0 = x_t;
params.d = d_t;

params.x_min = x_min;
params.x_max = x_max;

params.u_min = u_min;
params.u_max = u_max;

params.Q = Q;
params.R = R;

% Solve the MIP: branch and bound + cvxgen solver
% coder.extrinsic('csolve_mex')

% Set solver settings + Initialize status;
settings.max_iters = 100;
settings.verbose = 0;
settings.eps = 1e-4;
settings.resid_tol = 1e-4;

status.optval = -Inf;
status.gap = 0;
status.steps = 0;
status.converged = 0;
  
u_t = 0;

if horizon == 20
  % N = 20
  [vars, status] = csolve_mex_20(params, settings);
elseif horizon == 5
  % N = 5
  [vars, status] = csolve_mex_5(params, settings);
elseif horizon == 2
  % N = 2
  [vars, status] = csolve_mex_2(params, settings);
end
u_t = vars.u_0;
% if ~status.converged
%     warning('MPC not feasible !?')
% end
u_t = min(max(u_t,con.u_min_LK),con.u_max_LK); % physical saturate
