function [u_t] = controller_P(x_t, con, Dt, gains)

if nargin < 4
    Dt = 0.1;
end
u_t_unsaturated = gains*x_t;
u_t = max(min(con.u_max_LK, u_t_unsaturated),con.u_min_LK);
