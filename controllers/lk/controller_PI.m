function [xi_t, u_t] = controller_PI(x_t, xi_t, con, Dt, gains)

if nargin < 4
    Dt = 0.1;
end
xi_t = xi_t + Dt*x_t(1); 
u_t_unsaturated = gains*[x_t; xi_t]; % poles: 0.002; 0.6+0.4i; 0.6-0.4i; 0.4; 0.7

u_t = max(min(con.u_max_LK, u_t_unsaturated),con.u_min_LK);
