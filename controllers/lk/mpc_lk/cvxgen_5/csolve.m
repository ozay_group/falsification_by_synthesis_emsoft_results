% csolve  Solves a custom quadratic program very rapidly.
%
% [vars, status] = csolve(params, settings)
%
% solves the convex optimization problem
%
%   minimize(quad_form(x_1, Q) + quad_form(u_0, R) + quad_form(x_2, Q) + quad_form(u_1, R) + quad_form(x_3, Q) + quad_form(u_2, R) + quad_form(x_4, Q) + quad_form(u_3, R) + quad_form(x_5, Q) + quad_form(u_4, R))
%   subject to
%     x_1 - (A*x_0 + B*u_0 + d*E + K) == 0
%     x_2 - (A*x_1 + B*u_1 + d*E + K) == 0
%     x_3 - (A*x_2 + B*u_2 + d*E + K) == 0
%     x_4 - (A*x_3 + B*u_3 + d*E + K) == 0
%     x_5 - (A*x_4 + B*u_4 + d*E + K) == 0
%     x_0 == x0
%     x_min <= x_1
%     x_min <= x_2
%     x_min <= x_3
%     x_min <= x_4
%     x_1 <= x_max
%     x_2 <= x_max
%     x_3 <= x_max
%     x_4 <= x_max
%     u_min <= u_0
%     u_min <= u_1
%     u_min <= u_2
%     u_min <= u_3
%     u_min <= u_4
%     u_0 <= u_max
%     u_1 <= u_max
%     u_2 <= u_max
%     u_3 <= u_max
%     u_4 <= u_max
%
% with variables
%      u_0   1 x 1
%      u_1   1 x 1
%      u_2   1 x 1
%      u_3   1 x 1
%      u_4   1 x 1
%      x_0   4 x 1
%      x_1   4 x 1
%      x_2   4 x 1
%      x_3   4 x 1
%      x_4   4 x 1
%      x_5   4 x 1
%
% and parameters
%        A   4 x 4
%        B   4 x 1
%        E   4 x 1
%        K   4 x 1
%        Q   4 x 4    PSD
%        R   1 x 1    PSD
%        d   1 x 1
%    u_max   1 x 1
%    u_min   1 x 1
%       x0   4 x 1
%    x_max   4 x 1
%    x_min   4 x 1
%
% Note:
%   - Check status.converged, which will be 1 if optimization succeeded.
%   - You don't have to specify settings if you don't want to.
%   - To hide output, use settings.verbose = 0.
%   - To change iterations, use settings.max_iters = 20.
%   - You may wish to compare with cvxsolve to check the solver is correct.
%
% Specify params.A, ..., params.x_min, then run
%   [vars, status] = csolve(params, settings)
% Produced by CVXGEN, 2018-03-27 23:32:49 -0400.
% CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com.
% The code in this file is Copyright (C) 2006-2017 Jacob Mattingley.
% CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
% applications without prior written permission from Jacob Mattingley.

% Filename: csolve.m.
% Description: Help file for the Matlab solver interface.
