/* Produced by CVXGEN, 2018-03-27 23:37:47 -0400.  */
/* CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2017 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: matrix_support.c. */
/* Description: Support functions for matrix multiplication and vector filling. */
#include "solver.h"
void multbymA(double *lhs, double *rhs) {
  lhs[0] = -rhs[0]*(-params.B[0])-rhs[2]*(-params.A[0])-rhs[3]*(-params.A[4])-rhs[4]*(-params.A[8])-rhs[5]*(-params.A[12])-rhs[6]*(1);
  lhs[1] = -rhs[0]*(-params.B[1])-rhs[2]*(-params.A[1])-rhs[3]*(-params.A[5])-rhs[4]*(-params.A[9])-rhs[5]*(-params.A[13])-rhs[7]*(1);
  lhs[2] = -rhs[0]*(-params.B[2])-rhs[2]*(-params.A[2])-rhs[3]*(-params.A[6])-rhs[4]*(-params.A[10])-rhs[5]*(-params.A[14])-rhs[8]*(1);
  lhs[3] = -rhs[0]*(-params.B[3])-rhs[2]*(-params.A[3])-rhs[3]*(-params.A[7])-rhs[4]*(-params.A[11])-rhs[5]*(-params.A[15])-rhs[9]*(1);
  lhs[4] = -rhs[1]*(-params.B[0])-rhs[6]*(-params.A[0])-rhs[7]*(-params.A[4])-rhs[8]*(-params.A[8])-rhs[9]*(-params.A[12])-rhs[10]*(1);
  lhs[5] = -rhs[1]*(-params.B[1])-rhs[6]*(-params.A[1])-rhs[7]*(-params.A[5])-rhs[8]*(-params.A[9])-rhs[9]*(-params.A[13])-rhs[11]*(1);
  lhs[6] = -rhs[1]*(-params.B[2])-rhs[6]*(-params.A[2])-rhs[7]*(-params.A[6])-rhs[8]*(-params.A[10])-rhs[9]*(-params.A[14])-rhs[12]*(1);
  lhs[7] = -rhs[1]*(-params.B[3])-rhs[6]*(-params.A[3])-rhs[7]*(-params.A[7])-rhs[8]*(-params.A[11])-rhs[9]*(-params.A[15])-rhs[13]*(1);
  lhs[8] = -rhs[2]*(1);
  lhs[9] = -rhs[3]*(1);
  lhs[10] = -rhs[4]*(1);
  lhs[11] = -rhs[5]*(1);
}
void multbymAT(double *lhs, double *rhs) {
  lhs[0] = -rhs[0]*(-params.B[0])-rhs[1]*(-params.B[1])-rhs[2]*(-params.B[2])-rhs[3]*(-params.B[3]);
  lhs[1] = -rhs[4]*(-params.B[0])-rhs[5]*(-params.B[1])-rhs[6]*(-params.B[2])-rhs[7]*(-params.B[3]);
  lhs[2] = -rhs[0]*(-params.A[0])-rhs[1]*(-params.A[1])-rhs[2]*(-params.A[2])-rhs[3]*(-params.A[3])-rhs[8]*(1);
  lhs[3] = -rhs[0]*(-params.A[4])-rhs[1]*(-params.A[5])-rhs[2]*(-params.A[6])-rhs[3]*(-params.A[7])-rhs[9]*(1);
  lhs[4] = -rhs[0]*(-params.A[8])-rhs[1]*(-params.A[9])-rhs[2]*(-params.A[10])-rhs[3]*(-params.A[11])-rhs[10]*(1);
  lhs[5] = -rhs[0]*(-params.A[12])-rhs[1]*(-params.A[13])-rhs[2]*(-params.A[14])-rhs[3]*(-params.A[15])-rhs[11]*(1);
  lhs[6] = -rhs[0]*(1)-rhs[4]*(-params.A[0])-rhs[5]*(-params.A[1])-rhs[6]*(-params.A[2])-rhs[7]*(-params.A[3]);
  lhs[7] = -rhs[1]*(1)-rhs[4]*(-params.A[4])-rhs[5]*(-params.A[5])-rhs[6]*(-params.A[6])-rhs[7]*(-params.A[7]);
  lhs[8] = -rhs[2]*(1)-rhs[4]*(-params.A[8])-rhs[5]*(-params.A[9])-rhs[6]*(-params.A[10])-rhs[7]*(-params.A[11]);
  lhs[9] = -rhs[3]*(1)-rhs[4]*(-params.A[12])-rhs[5]*(-params.A[13])-rhs[6]*(-params.A[14])-rhs[7]*(-params.A[15]);
  lhs[10] = -rhs[4]*(1);
  lhs[11] = -rhs[5]*(1);
  lhs[12] = -rhs[6]*(1);
  lhs[13] = -rhs[7]*(1);
}
void multbymG(double *lhs, double *rhs) {
  lhs[0] = -rhs[6]*(-1);
  lhs[1] = -rhs[7]*(-1);
  lhs[2] = -rhs[8]*(-1);
  lhs[3] = -rhs[9]*(-1);
  lhs[4] = -rhs[6]*(1);
  lhs[5] = -rhs[7]*(1);
  lhs[6] = -rhs[8]*(1);
  lhs[7] = -rhs[9]*(1);
  lhs[8] = -rhs[0]*(-1);
  lhs[9] = -rhs[1]*(-1);
  lhs[10] = -rhs[0]*(1);
  lhs[11] = -rhs[1]*(1);
}
void multbymGT(double *lhs, double *rhs) {
  lhs[0] = -rhs[8]*(-1)-rhs[10]*(1);
  lhs[1] = -rhs[9]*(-1)-rhs[11]*(1);
  lhs[2] = 0;
  lhs[3] = 0;
  lhs[4] = 0;
  lhs[5] = 0;
  lhs[6] = -rhs[0]*(-1)-rhs[4]*(1);
  lhs[7] = -rhs[1]*(-1)-rhs[5]*(1);
  lhs[8] = -rhs[2]*(-1)-rhs[6]*(1);
  lhs[9] = -rhs[3]*(-1)-rhs[7]*(1);
  lhs[10] = 0;
  lhs[11] = 0;
  lhs[12] = 0;
  lhs[13] = 0;
}
void multbyP(double *lhs, double *rhs) {
  /* TODO use the fact that P is symmetric? */
  /* TODO check doubling / half factor etc. */
  lhs[0] = rhs[0]*(2*params.R[0]);
  lhs[1] = rhs[1]*(2*params.R[0]);
  lhs[2] = 0;
  lhs[3] = 0;
  lhs[4] = 0;
  lhs[5] = 0;
  lhs[6] = rhs[6]*(2*params.Q[0])+rhs[7]*(2*params.Q[4])+rhs[8]*(2*params.Q[8])+rhs[9]*(2*params.Q[12]);
  lhs[7] = rhs[6]*(2*params.Q[1])+rhs[7]*(2*params.Q[5])+rhs[8]*(2*params.Q[9])+rhs[9]*(2*params.Q[13]);
  lhs[8] = rhs[6]*(2*params.Q[2])+rhs[7]*(2*params.Q[6])+rhs[8]*(2*params.Q[10])+rhs[9]*(2*params.Q[14]);
  lhs[9] = rhs[6]*(2*params.Q[3])+rhs[7]*(2*params.Q[7])+rhs[8]*(2*params.Q[11])+rhs[9]*(2*params.Q[15]);
  lhs[10] = rhs[10]*(2*params.Q[0])+rhs[11]*(2*params.Q[4])+rhs[12]*(2*params.Q[8])+rhs[13]*(2*params.Q[12]);
  lhs[11] = rhs[10]*(2*params.Q[1])+rhs[11]*(2*params.Q[5])+rhs[12]*(2*params.Q[9])+rhs[13]*(2*params.Q[13]);
  lhs[12] = rhs[10]*(2*params.Q[2])+rhs[11]*(2*params.Q[6])+rhs[12]*(2*params.Q[10])+rhs[13]*(2*params.Q[14]);
  lhs[13] = rhs[10]*(2*params.Q[3])+rhs[11]*(2*params.Q[7])+rhs[12]*(2*params.Q[11])+rhs[13]*(2*params.Q[15]);
}
void fillq(void) {
  work.q[0] = 0;
  work.q[1] = 0;
  work.q[2] = 0;
  work.q[3] = 0;
  work.q[4] = 0;
  work.q[5] = 0;
  work.q[6] = 0;
  work.q[7] = 0;
  work.q[8] = 0;
  work.q[9] = 0;
  work.q[10] = 0;
  work.q[11] = 0;
  work.q[12] = 0;
  work.q[13] = 0;
}
void fillh(void) {
  work.h[0] = -params.x_min[0];
  work.h[1] = -params.x_min[1];
  work.h[2] = -params.x_min[2];
  work.h[3] = -params.x_min[3];
  work.h[4] = params.x_max[0];
  work.h[5] = params.x_max[1];
  work.h[6] = params.x_max[2];
  work.h[7] = params.x_max[3];
  work.h[8] = -params.u_min[0];
  work.h[9] = -params.u_min[0];
  work.h[10] = params.u_max[0];
  work.h[11] = params.u_max[0];
}
void fillb(void) {
  work.b[0] = params.d[0]*params.E[0]+params.K[0];
  work.b[1] = params.d[0]*params.E[1]+params.K[1];
  work.b[2] = params.d[0]*params.E[2]+params.K[2];
  work.b[3] = params.d[0]*params.E[3]+params.K[3];
  work.b[4] = params.d[0]*params.E[0]+params.K[0];
  work.b[5] = params.d[0]*params.E[1]+params.K[1];
  work.b[6] = params.d[0]*params.E[2]+params.K[2];
  work.b[7] = params.d[0]*params.E[3]+params.K[3];
  work.b[8] = params.x0[0];
  work.b[9] = params.x0[1];
  work.b[10] = params.x0[2];
  work.b[11] = params.x0[3];
}
void pre_ops(void) {
}
