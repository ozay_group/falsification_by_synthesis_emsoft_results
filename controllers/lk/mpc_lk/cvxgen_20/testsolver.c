/* Produced by CVXGEN, 2018-01-28 21:10:35 -0500.  */
/* CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2017 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: testsolver.c. */
/* Description: Basic test harness for solver.c. */
#include "solver.h"
Vars vars;
Params params;
Workspace work;
Settings settings;
#define NUMTESTS 0
int main(int argc, char **argv) {
  int num_iters;
#if (NUMTESTS > 0)
  int i;
  double time;
  double time_per;
#endif
  set_defaults();
  setup_indexing();
  load_default_data();
  /* Solve problem instance for the record. */
  settings.verbose = 1;
  num_iters = solve();
#ifndef ZERO_LIBRARY_MODE
#if (NUMTESTS > 0)
  /* Now solve multiple problem instances for timing purposes. */
  settings.verbose = 0;
  tic();
  for (i = 0; i < NUMTESTS; i++) {
    solve();
  }
  time = tocq();
  printf("Timed %d solves over %.3f seconds.\n", NUMTESTS, time);
  time_per = time / NUMTESTS;
  if (time_per > 1) {
    printf("Actual time taken per solve: %.3g s.\n", time_per);
  } else if (time_per > 1e-3) {
    printf("Actual time taken per solve: %.3g ms.\n", 1e3*time_per);
  } else {
    printf("Actual time taken per solve: %.3g us.\n", 1e6*time_per);
  }
#endif
#endif
  return 0;
}
void load_default_data(void) {
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.Q[0] = 1.5507979025745755;
  params.Q[4] = 0;
  params.Q[8] = 0;
  params.Q[12] = 0;
  params.Q[1] = 0;
  params.Q[5] = 1.7081478226181048;
  params.Q[9] = 0;
  params.Q[13] = 0;
  params.Q[2] = 0;
  params.Q[6] = 0;
  params.Q[10] = 1.2909047389129444;
  params.Q[14] = 0;
  params.Q[3] = 0;
  params.Q[7] = 0;
  params.Q[11] = 0;
  params.Q[15] = 1.510827605197663;
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.R[0] = 1.8929469543476547;
  params.A[0] = 1.5851723557337523;
  params.A[1] = -1.497658758144655;
  params.A[2] = -1.171028487447253;
  params.A[3] = -1.7941311867966805;
  params.A[4] = -0.23676062539745413;
  params.A[5] = -1.8804951564857322;
  params.A[6] = -0.17266710242115568;
  params.A[7] = 0.596576190459043;
  params.A[8] = -0.8860508694080989;
  params.A[9] = 0.7050196079205251;
  params.A[10] = 0.3634512696654033;
  params.A[11] = -1.9040724704913385;
  params.A[12] = 0.23541635196352795;
  params.A[13] = -0.9629902123701384;
  params.A[14] = -0.3395952119597214;
  params.A[15] = -0.865899672914725;
  params.B[0] = 0.7725516732519853;
  params.B[1] = -0.23818512931704205;
  params.B[2] = -1.372529046100147;
  params.B[3] = 0.17859607212737894;
  params.E[0] = 1.1212590580454682;
  params.E[1] = -0.774545870495281;
  params.E[2] = -1.1121684642712744;
  params.E[3] = -0.44811496977740495;
  params.d[0] = 1.7455345994417217;
  params.K[0] = 1.9039816898917352;
  params.K[1] = 0.6895347036512547;
  params.K[2] = 1.6113364341535923;
  params.K[3] = 1.383003485172717;
  params.x0[0] = -0.48802383468444344;
  params.x0[1] = -1.631131964513103;
  params.x0[2] = 0.6136436100941447;
  params.x0[3] = 0.2313630495538037;
  params.x_min[0] = -0.5537409477496875;
  params.x_min[1] = -1.0997819806406723;
  params.x_min[2] = -0.3739203344950055;
  params.x_min[3] = -0.12423900520332376;
  params.x_max[0] = -0.923057686995755;
  params.x_max[1] = -0.8328289030982696;
  params.x_max[2] = -0.16925440270808823;
  params.x_max[3] = 1.442135651787706;
  params.u_min[0] = 0.34501161787128565;
  params.u_max[0] = -0.8660485502711608;
}
