# falsification_by_synthesis

This repository contains algorithms and examples related to the following paper in the proceedings of EMSOFT 2018:

Using control synthesis to generate corner cases: A case study on autonomous driving, 
Authors: Glen Chou, Yunus Sahin, Liren Yang, Kwesi Rutledge, Petter Nilsson, Necmiye Ozay
(The first three authors contributed equally.) [link to paper](https://doi.org/10.1109/TCAD.2018.2858464)

## Controllers

/controllers contains unsupervised controllers used in our experiments:

* /controllers/acc contains the model predictive controllers used for adaptive cruise control (ACC), compiled with lookahead horizons 2, 5, and 20. The P/PI controllers for ACC are embedded in the EMSOFT results run scripts.
* /controllers/commaAI contains the CommaAI controllers used for both ACC and lane-keeping (LK).
* /controllers/lk contains the LK controllers (P/PI/MPC).

## Invariant set data

/data contains invariant sets and initial condition samples from those invariant sets. Information specific to mat file are detailed in corresponding .txt files:

* /data/acc: contains the ACC invariant set (safe_set_ACC_consolidated) as well as the backward reachable sets (BRS) used for input generation (BR_...).
* /data/acc/acc_samples: contains initial condition (IC) samples taken from the ACC invariant set.
* /data/lk: contains the LK invariant set (CInv_LK) as well as the backward reachable sets (BRS) used for input generation (BR_...). The LK IC samples are in the "lk_..." mat files.

## Scripts reproducing EMSOFT results
To reproduce the EMSOFT results with relation to ACC, run acc_sweep.m. To reproduce the EMSOFT results with relation to LK, run run_lk_experiments.m.

## Sampling scripts
Scripts used to generate the samples from the ACC/LK invariant sets are included in /extra/acc and /extra/lk, respectively. Furthermore, /extra/lk/generate_BR_x_files and /extra/lk/generate_BR_xd_files contain files to generate the LK backward reachable sets.

## Core files
/lib contains the core files implementing our algorithms:

* /lib/acc:

	* /lib/acc/acc_comma_ai... implement the ACC falsification framework
	* /lib/acc/acc_inverse_game.m implements the dual game input generation method
	* /lib/acc/acc_sweep_sTaliro... implement the sTaliro tests
	* Other files in this folder are just helper functions for running the falsification framework.
	
* /lib/ellipsoid:
	* Contains files for the ellipsoid level set method for computing inputs that steer the system to the winning set of the dual game.
	
* /lib/lk:

	* /lib/lk/inverse_game_LK_fast.m contains the dual game code for LK.
	* /lib/lk/supervisor_... implements the supervisor for LK (the supervisor for ACC is embedded in the falsification script)
	* /lib/lk/helper and any other unmentioned files in /lib/lk contain helper functions for implementing the falsification framework.
	
## Acknowledgments
This work was supported by the Toyota Research Institute (“TRI”). TRI provided funds to assist 
the authors with their research but this article solely reflects the opinions and conclusions 
of its authors and not TRI or any other Toyota entity.
