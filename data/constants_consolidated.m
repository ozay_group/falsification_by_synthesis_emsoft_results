function [con] = constants_consolidated()

  % sampling time
  con.dT = 0.1;
  con.dt = 0.1;
  % mass
  con.mass = (2923/2.205) + 136;
  con.m = con.mass; 
  % ACC constants 
  % car dynamics
  con.f0 = 51;
  con.f1 = 1.2567;
  con.f2 = 0.4342;
  con.g = 9.82;
  % linearization
  con.lin_speed = 20;
  con.f0_bar = con.f0 - con.f2*con.lin_speed^2;
  con.f1_bar = con.f1 + 2*con.f2*con.lin_speed;
  % domain specs
  con.v_min = 0;
  con.v_max = 25;
  con.h_min = 0;
  % assumptions
  con.umin_ACC = -0.3*con.mass*con.g;
  con.umax_ACC = 0.2*con.mass*con.g;
  con.vl_min = 0;
  con.vl_max = 25;
  con.al_min = -0.97; % m/s^2
  con.al_max = 0.65;  % m/s^2
  % specifications
  con.h_min = 4;   % minimum distance %since safety spec overwriting the state bounds is fine
  con.tau_min = 1.7; % minimum headway
  con.tau_des = 2.1;
  con.v_des_min = 19;
  con.v_des_max = 21;
  con.v_des = 20;
    
  % LK Constants
  % nominal speed
  con.u0 = 20; %m/s
  % Vehicle parameters
  con.Iz= 2500; % kgm^2
  con.a=1.08; % m
  con.b = 1.62; % m
  con.L=2.7;
  % Maximal assumed road curvature as alpha*g
  con.alpha_ass = 0.32;
  % Road curvature in simulation
  con.alpha_road = 0.3;
  % Tire parameters
  con.Caf = 85400; % N/rad 
  con.Car = 90000; % N/rad
  con.F_yfmax = 1500; % not sure about this one (took from legacy code)
  con.d_max = con.u0/((con.u0^2)/(0.9*con.alpha_road*con.g)); %bounds on r_d
  con.d_min = -con.d_max; 
  con.rmax = con.a*con.u0*con.F_yfmax/(con.b^2*con.Car);
  con.x_max = [0.9; 1; 0.15; 2*con.rmax]; %state bounds
  con.x_min = -con.x_max;
  % bounds on \delta_f
  con.u_max_LK = deg2rad(15); 
  con.u_min_LK = -con.u_max_LK;
  con.align_scale = 2/con.u0;
  con.steer_scale = 2;
end