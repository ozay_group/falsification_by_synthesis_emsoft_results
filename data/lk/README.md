# falsification_by_synthesis

The data directory is the only directory where binary files are allow (i.e., 
.mat files). Each file.mat file should be accompanied with a file_info.txt 
(with this naming convention) that explains the content of file and how it
is generated. Updating a data file without updating the corresponding info 
file is not allowed.

